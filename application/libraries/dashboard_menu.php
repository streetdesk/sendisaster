<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Dashboard_menu {

    public function menu()
    {
      $CI =& get_instance();
      $ad_access = $CI->session->userdata('ad_access');
      if($ad_access == 4):
          echo '
            <ul class="sms-options-inner clearfix">
               <li><a href="'.base_url('Dashboard/request').'" id="request">Request</a></li>
            </ul>
           ';
      else:
          echo '
            <ul class="sms-options-inner clearfix">
               <li><a href="'.base_url('Dashboard/') .'" id="disseminate">Disseminate</a></li>
               <li><a href="'.base_url('Dashboard/request').'" id="request">Request</a></li>
               <li><a href="'.base_url('Dashboard/floodreport').'" id="floodanalysis">Flood Report Analysis</a></li>
            </ul>
           ';
      
      endif;
        
    }
    
}

   
//
//echo '
//            <ul class="sms-options-inner clearfix">
//               <li><a href="'.base_url('Dashboard/') .'" id="disseminate">Disseminate</a></li>
//               <li><a href="'.base_url('Dashboard/request').'" id="request">Request</a></li>
//               <li><a href="'.base_url('Dashboard/floodreport').'" id="floodanalysis">Flood Report Analysis</a></li>
//               <li><a href="'.base_url('Dashboard/barangay').'" id="barangay">Barangay</a></li>
//            </ul>
//           ';