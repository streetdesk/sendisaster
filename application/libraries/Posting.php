<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Posting {

    public function latest_post()
    {
        
        $CI =& get_instance();
        
        $CI->load->helper('url');
        
        $sql = $CI->db->query('SELECT * FROM post ORDER BY post_id DESC');
        foreach ($sql->result() as $row):
            $ad_id = $row->ad_id;
            $post_id = $row->post_id;
            $post_title = $row->post_title;
            $post_desc = $row->post_description;
            $post_date = $row->post_date;
            $cat_id = $row->cat_id;
            $post_fet_img = $row->post_fet_img;
            $feat_img = "";
            $mun_name = "";
            $mun_image = "";
            if(!empty($post_fet_img)){
                $feat_img = '
                            <div class="gen-update-image">
                                <img src="'.base_url($post_fet_img).'" alt="" />
                            </div>';
            }
            $sqlz = $CI->db->query("SELECT * from administrator where ad_id = ".$ad_id);
            foreach ($sqlz->result() as $rowz):
                $mun_id  = $rowz->mun_id;
            endforeach;
            
            $sqlx = $CI->db->query("SELECT * from municipals where mun_id =".$mun_id);
            foreach($sqlx->result() as $rowx):
                $mun_name = $rowx->mun_name;
                $mun_image = base_url($rowx->mun_image);
            endforeach;
            
            echo '
                <ul class="gen-update-inner clearfix">
                    <li class="gen-update-header clearfix">
                        <ul class="inner-header clearfix">
                            <li class="img-holder clearfix"><img src="'.$mun_image.'"/></li>
                            <li class="post-header"><h4>'.$mun_name.'</h4><p>'.$row->post_date.'</p></li>
                        </ul>
                    </li>

                    <li class="gen-update-description">
                        <h3 class="clearfix">'.$post_title.'</h3>
                        '.$feat_img.'
                        <article class="clearfix">
                            <p class="clearfix">'.$post_desc.'</p>
                        </article>
                    </li>
                </ul>
                 '; 
        endforeach;
        
    }
    public function latest_weather_feed()
    {
//        $CI =& get_instance();
//        
//        $sql = $CI->db->query('SELECT * FROM post where cat_id = 2 ORDER BY post_id DESC LIMIT 1');
//        $num_rows = $sql->num_rows();
//        
//        if($num_rows != 0):
//            foreach($sql->result() as $row):
//                echo $row->post_description;
//            endforeach;
//        endif;
        $page_id = 'PAGASA.DOST.GOV.PH';
        $access_token = '1483327495282124|91fe77986506614583442c4d2ec5a9ff';
        //Get the JSON
        $json_object = @file_get_contents('https://graph.facebook.com/' . $page_id . 
        '/posts?access_token=' . $access_token);
        //Interpret data
        $fbdata = json_decode($json_object);

       // echo $json_object;
        $num = 0;
        $posts ="";
        foreach ($fbdata->data as $post )
        {
            if(isset($post->message)){
//                $posts .= '
//                      <ul class="gen-update-inner clearfix">
//                            <li class="gen-update-header clearfix">
//                                <ul class="inner-header clearfix">
//                                    <li class="img-holder clearfix"><img src="none.jpg"/></li>
//                                    <li class="post-title"><h4>'.$post->from->name.'</h4><p>01/01/2015</p></li>
//                                </ul>
//                            </li>
//                            <li class="gen-update-description">
//                                '.$post->message.'
//                            </li>
//                        </ul>
//                      ';
                $posts .= '
                            <ul class="feed-holder clearfix">
                                        <li class="feed-image">
                                            <img src="'.base_url('images/department/Pagasa_logo.png').'" alt="">
                                        </li>
                                        <li class="feed-title">
                                            <p>'.$post->from->name.'</p>
                                        </li>
                                        <li class="feed-desc"><p>'.substr($post->message,20).'</p> </li>
                                    </ul>
                          ';
            }
       
        }
        echo $posts;

    }
    public function latest_weather_advisory()
    {
        $CI =& get_instance();
        
        $sql = $CI->db->query('SELECT * FROM post where cat_id = 3 ORDER BY post_id DESC LIMIT 1');
        $num_rows = $sql->num_rows();
        
        if($num_rows != 0):
            foreach($sql->result() as $row):
                echo $row->post_description;
            endforeach;
        endif;
    }
    public function latest_hydro_warnings()
    {
        $CI =& get_instance();
        
        $sql = $CI->db->query('SELECT * FROM post where cat_id = 4 ORDER BY post_id DESC LIMIT 1');
        $num_rows = $sql->num_rows();
        
        if($num_rows != 0):
            foreach($sql->result() as $row):
                echo $row->post_description;
            endforeach;
        endif;
    }

    public function latest_climate_advisory()
    {
        $CI =& get_instance();
        
        $sql = $CI->db->query('SELECT * FROM post where cat_id = 5 ORDER BY post_id DESC LIMIT 1');
        $num_rows = $sql->num_rows();
        
        if($num_rows != 0):
            foreach($sql->result() as $row):
                echo $row->post_description;
            endforeach;
        endif;
    }      
}

/* End of file Posting.php */