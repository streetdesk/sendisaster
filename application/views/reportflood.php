
    
       
   <style>
       #map-canvas {
        height: 550px;
        width:1349px;
        margin: 0px;
        padding: 0px;
        border:solid 0px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<!--
    <script>
        var map;
        var citymap = {
                   center: new google.maps.LatLng(15.9904084, 120.363805),
                   population: 2237
               };     
               
        var cityCircle;
   
          
        function initialize() {
          var image = 'http://aminiherbals.com/images/amini_yellow_circle.png';
            
            
          var mapOptions = {
            zoom: 10,
            center: new google.maps.LatLng(15.998431, 120.229793)
          };
          
         
          map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);
        
          var populationOptions = {
              strokeColor: '#17ff00',
              strokeOpacity: 0.75,
              strokeWeight: 1,
              fillColor: '#00f75c',
              fillOpacity: 0.3,
              map: map,
              center: citymap.center,
              radius: Math.sqrt(citymap.population) * 100
            };
            // Add the circle for this city to the map.
            cityCircle = new google.maps.Circle(populationOptions);
          
          google.maps.event.addListener(marker, 'click', function() {
            map.setZoom(8);
            map.setCenter(marker.getPosition());
          });


        }

google.maps.event.addDomListener(window, 'load', initialize);



    </script>
-->


<script>
// [START region_initialization]
// This example creates a custom overlay called USGSOverlay, containing
// a U.S. Geological Survey (USGS) image of the relevant area on the map.

// Set the custom overlay object's prototype to a new instance
// of OverlayView. In effect, this will subclass the overlay class.
// Note that we set the prototype to an instance, rather than the
// parent class itself, because we do not wish to modify the parent class.

var overlay;
USGSOverlay.prototype = new google.maps.OverlayView();

// Initialize the map and the custom overlay.

function initialize() {
  var mapOptions = {
    zoom: 10,
    center: new google.maps.LatLng(15.998431, 120.229793),
    mapTypeId: google.maps.MapTypeId.SATELLITE
  };
  
  

  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var swBound = new google.maps.LatLng(15.752306, 119.567653);
  var neBound = new google.maps.LatLng(16.534125, 120.702787);
  var bounds = new google.maps.LatLngBounds(swBound, neBound);

  // The photograph is courtesy of the U.S. Geological Survey.
//  var srcImage = 'https://developers.google.com/maps/documentation/javascript/';
//  srcImage += 'examples/full/images/talkeetna.png';

var srcImage = '<?php echo base_url('images/Hazard_1.jpg'); ?>';

  // The custom USGSOverlay object contains the USGS image,
  // the bounds of the image, and a reference to the map.
  overlay = new USGSOverlay(bounds, srcImage, map);
}
// [END region_initialization]

// [START region_constructor]
/** @constructor */
function USGSOverlay(bounds, image, map) {

  // Initialize all properties.
  this.bounds_ = bounds;
  this.image_ = image;
  this.map_ = map;

  // Define a property to hold the image's div. We'll
  // actually create this div upon receipt of the onAdd()
  // method so we'll leave it null for now.
  this.div_ = null;

  // Explicitly call setMap on this overlay.
  this.setMap(map);
}
// [END region_constructor]

// [START region_attachment]
/**
 * onAdd is called when the map's panes are ready and the overlay has been
 * added to the map.
 */
USGSOverlay.prototype.onAdd = function() {

  var div = document.createElement('div');
  div.style.borderStyle = 'none';
  div.style.borderWidth = '0px';
  div.style.position = 'absolute';

  // Create the img element and attach it to the div.
  var img = document.createElement('img');
  img.src = this.image_;
  img.style.width = '130%';
  img.style.height = '130%';
  img.style.position = 'absolute';
  img.style.opacity = '0.75';
  div.appendChild(img);

  this.div_ = div;

  // Add the element to the "overlayLayer" pane.
  var panes = this.getPanes();
  panes.overlayLayer.appendChild(div);
};
// [END region_attachment]

// [START region_drawing]
USGSOverlay.prototype.draw = function() {

  // We use the south-west and north-east
  // coordinates of the overlay to peg it to the correct position and size.
  // To do this, we need to retrieve the projection from the overlay.
  var overlayProjection = this.getProjection();

  // Retrieve the south-west and north-east coordinates of this overlay
  // in LatLngs and convert them to pixel coordinates.
  // We'll use these coordinates to resize the div.
  var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
  var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

  // Resize the image's div to fit the indicated dimensions.
  var div = this.div_;
  div.style.left = sw.x + 'px';
  div.style.top = ne.y + 'px';
  div.style.width = (ne.x - sw.x) + 'px';
  div.style.height = (sw.y - ne.y) + 'px';
};
// [END region_drawing]

// [START region_removal]
// The onRemove() method will be called automatically from the API if
// we ever set the overlay's map property to 'null'.
USGSOverlay.prototype.onRemove = function() {
  this.div_.parentNode.removeChild(this.div_);
  this.div_ = null;
};
// [END region_removal]

google.maps.event.addDomListener(window, 'load', initialize);

    </script>


<section class="clearfix">
   <ul class="content-map-wrap">
       <li>
<!--
            <div class="report-form clearfix shadow">
               <h1>Report A flood</h1>
               <ul class="form-inner-holder"> 
                   <li class="input-label">Barangay:</li>
                   <li class="input-holder"> 
                       <select class="input-text"></select>
                   </li>
               </ul>
               <ul class="form-inner-holder"> 
                   <li class="input-label">Date:</li>
                   <li class="input-holder"> 
                    <?php 
//                            echo $this->data['birth_date_year'] = buildYearDropdown('birth_date_year', $this->input->post('birth_date_year'));
//                            echo $this->data['birth_date_month'] = buildMonthDropdown('birth_date_month', $this->input->post('birth_date_month'));
//                            echo $this->data['birth_date_day'] = buildDayDropdown('birth_date_day', $this->input->post('birth_date_day'));
                       ?>
                   </li>
               </ul>
               <ul class="form-inner-holder"> 
                   <li class="input-label">Time:</li>
                   <li class="input-holder"> 
                    <?php 
//                            echo $this->data['birth_date_year'] = buildYearDropdown('birth_date_year', $this->input->post('birth_date_year'));
//                            echo $this->data['birth_date_month'] = buildMonthDropdown('birth_date_month', $this->input->post('birth_date_month'));
//                            echo $this->data['birth_date_day'] = buildDayDropdown('birth_date_day', $this->input->post('birth_date_day'));
                       ?>
                   </li>
               </ul>
               <ul class="form-inner-holder"> 
                   <li class="input-label">Description:</li>
                   <li class="input-holder"> 
                        <textarea class="rep-desc input-text" placeholder="Enter your Description"></textarea>
                   </li>
               </ul>
               <ul class="form-inner-holder"> 
                   <li class="input-holder"> 
                        <button class="rep-button btn">Report</button>
                   </li>
               </ul>
            </div>
-->
             <div id="map-canvas" ></div>
       </li> 
   </ul>
</section>
    
 