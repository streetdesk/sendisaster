 <?php 
        $admin ='';        
        $sql = $this->db->query("SELECT * from administrator where ad_id = ".$ad_id);
        foreach($sql->result() as $row):
                $admin = array(
                      'ad_id'=>$row->ad_id,
                      'cn_id'=>$row->cn_id,
                      'mun_id'=>$row->mun_id,
                      'dept_id'=>$row->dept_id,
                      'ad_mail'=>$row->ad_mail,
                      'ad_username'=>$row->ad_username,
                      'ad_password'=>$row->ad_password,
                      'ad_lastname'=>$row->ad_lastname,
                      'ad_middlename'=>$row->ad_middlename,
                      'ad_firstname'=>$row->ad_firstname,
                      'ad_contact_no'=>$row->ad_contact_no,
                      'ad_address_no'=>$row->ad_address_no,
                      'ad_address_brgy'=>$row->ad_address_brgy,
                      'ad_province'=>$row->ad_province,
                      'ad_access'=>$row->ad_access
                      );      
        endforeach;
        
        ?>
        <?php 
                $municipal = "";
                $mun_ids = $admin['mun_id'];  
                
            $sqlx = $this->db->query("SELECT * from municipals where mun_id = ".$mun_ids);
            foreach($sqlx->result() as $rowx):
                    $municipal = array(
                                       'mun_id' =>$rowx->mun_id,
                                       'mun_zcode' =>$rowx->mun_zcode,
                                       'mun_name' =>$rowx->mun_name,
                                       'mun_image' =>$rowx->mun_image
                                       );
            endforeach;
        ?>
        <section class="clearfix">
           <ul class="content-wrap">
               <li class="left-content">
                   <h2 class="font_reg">Edit Profile Settings</h2>
                   <form action="<?php echo base_url('profilesettings/saveprofile') ?>" method="POST">
                   <ul class="inner-wrap clearfix">
                       <li class="sms-viewer clearfix">
                          <h3>Name:</h3>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Last Name: </p>
                                       <input type="text" id="message" name="lname" class="input-text-two clearfix"  value="<?php echo $admin['ad_lastname']; ?>"/>
                                  </li>
                              </ul>
                          </div>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Firstname Name: </p>
                                       <input type="text" id="message" name="fname" class="input-text-two clearfix"  value="<?php echo $admin['ad_firstname']; ?>"/>
                                  </li>
                              </ul>
                          </div>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Middle Name: </p>
                                       <input type="text" id="message" name="mname" class="input-text-two clearfix"  value="<?php echo $admin['ad_middlename']; ?>"/>
                                  </li>
                              </ul>
                          </div>
                          <h3>Phone Number</h3>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Cell Number: </p>
                                       <input type="text" id="message" name="cnumber" class="input-text-two clearfix"  value="<?php echo $admin['ad_contact_no']; ?>"/>
                                  </li>
                              </ul>
                          </div>
                          <h3>Address</h3>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Address No.: </p>
                                       <input type="text" id="message" name="addno" class="input-text-two clearfix"  value="<?php echo $admin['ad_address_no']; ?>"/>
                                  </li>
                              </ul>
                          </div>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Barangay: </p>
                                       <input type="text" id="message" name="brgy" class="input-text-two clearfix"  value="<?php echo $admin['ad_address_brgy']; ?>"/>
                                  </li>
                              </ul>
                          </div>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Province: </p>
                                       <input type="text" id="Save Changes" name="province" class="input-text-two clearfix"  value="<?php echo $admin['ad_province']; ?>"/>
                                  </li>
                              </ul>
                          </div>
                          <ul class="command-wrap clearfix" id="command">
                              <li id="m">
                                  <input type="submit" name="Save" value="Save Changes" class="btn" id="send-message"/>
                                  <input type="hidden" name="temp_dept_id" id="temp_dept_id" value="" />
                                  <input type="hidden" name="cat_id" id="cat_ids" value="" />
                              </li>
                              <li id="g" hidden><img id="loader" src="<?php echo base_url('img/loading.gif'); ?>" width="25" height="25" /></li>
                          </ul>
                       </li>
                   </ul>
                   </form>
               </li>
               <li class="right-content">
                  <div class="weather-holder clearfix">
                    <a href="http://www.accuweather.com/en/ph/pangasinan/764649/weather-forecast/764649" class="aw-widget-legal">
<!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
-->
</a><div id="awtd1428050678124" class="aw-widget-36hour"  data-locationkey="764649" data-unit="c" data-language="en-us" data-useip="false" data-uid="awtd1428050678124" data-editlocation="false"></div><script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>

                  </div>
               </li>
           </ul>
        </section>
