<?php 
    $num = 1;
    $ad_id = $this->session->userdata('ad_id');
    $req_id = $_GET['req_id'].'';
    $msg_id = $_GET['msg_id'];
    if(empty($_GET['req_id'])){ show_404(); }       
?>    
        <section class="clearfix">
           <ul class="content-wrap">
               <li class="left-content">
                   <h2 class="font_reg">Dashboard</h2>
                   <ul class="inner-wrap clearfix">
                       <li class="sms-options clearfix">
                          <?php $this->dashboard_menu->menu(); ?>
                       </li>
                       <li class="sms-viewer clearfix">
                          <h3>Issue Request</h3>
                          <form action="<?php echo base_url('Dashboard/approverequest'); ?>" method="post" id="approvaling">
                          <div class="messaging-wrap clearfix">
                              <ul class="box-req box-holder clearfix" >
                                  
                              <?php $sql = $this->db->query("SELECT * FROM `request` WHERE req_id =  ".$req_id) ;
                                    foreach($sql->result() as $row):
                                        $brgy_id = $row->brgy_id;
                                        $req_id = $row->req_id;
                                        $mobile_number = $row->mobile_number;
                                        $rtype_id = $row->req_type;
                                        $req_details = $row->req_details;
                                        $time_date = $row->req_date.' '.$row->req_time;
                                        $req_stat = $row->req_stat;
                                    endforeach;
                                    $sql_req = $this->db->query("SELECT * FROM `request_type` WHERE rtype_id = ".$rtype_id);
                                        foreach($sql_req->result() as $rowq):
                                            $rtype_name = $rowq->rtype_name;
                                        endforeach;
                                    $sql_brgy = $this->db->query("SELECT * FROM `barangay` WHERE brgy_id = ".$brgy_id);
                                        foreach($sql_brgy->result() as $rowb):
                                            $brgy_name = $rowb->brgy_name;
                                            $mun_id = $rowb->mun_id;        
                                        endforeach;
                                        
                                        $sql_mun = $this->db->query("SELECT * FROM `municipals` WHERE mun_id =".$mun_id);    
                                        foreach($sql_mun->result() as $rowm):
                                            $mun_name = $rowm->mun_name;    
                                        endforeach;
                                    
                                    echo '<h3>Request At The Municipal Agency</h3>
                                         <p><b>Sender:</b>'.$mobile_number.'</p>
                                         <p><b>Request Type:</b>'.$rtype_name.'</p>
                                         <p><b>Location:</b> Brgy. '.$brgy_name.', '.$mun_name.', Pangasinan</p>
                                         <p><b>Description</b>'.$req_details.'</p>
                                         ';
                              ?>
                              </ul>
                              <ul class="box-req box-holder clearfix" >
                                   <?php
                                       $sqlx = $this->db->query("SELECT * FROM messages WHERE req_id =".$req_id);
                                       foreach($sqlx->result() as $rowx):
                                           $ad_id = $rowx->ad_id;
                                           $req_id = $rowx->req_id;
                                           $msg_id = $rowx->msg_id;
                                           $msg_to = $rowx->msg_to;
                                           $msg_details = $rowx->msg_details;
                                           $msg_date = $rowx->msg_date;
                                           $msg_time = $rowx->msg_time;
                                       endforeach;
                                       ?>
                                       <?php
                                              $sql_sender = $this->db->query("SELECT * FROM administrator WHERE ad_id =".$ad_id);
                                              foreach($sql_sender->result() as $row_sender):
                                                      $smun_id = $row_sender->mun_id;
                                                      $sdept_id = $row_sender->dept_id;
                                              endforeach;
                                          ?>
                                         <?php
                                              $sql_mun = $this->db->query("SELECT * FROM municipals WHERE mun_id =".$smun_id);
                                              foreach($sql_mun->result() as $row_mun):
                                                      $smun_name = $row_mun->mun_name;
                                              endforeach;
                                          ?>
                                          <?php
                                              $sql_dept = $this->db->query("SELECT * FROM department WHERE dept_id =".$sdept_id);
                                              foreach($sql_dept->result() as $row_dept):
                                                      $sdept_name = $row_dept->dept_name;
                                              endforeach;
                                          ?>
                                       <?php
                                       echo '<h3>Message At The Municipal Agency</h3>
                                         <p><b>Sender:</b>'.$smun_name.' - '.$sdept_name.'</p>
                                         <p><b>Request Type:</b>'.$rtype_name.'</p>
                                         <p><b>Location:</b> Brgy. '.$brgy_name.', '.$mun_name.', Pangasinan</p>
                                         <p><b>Description</b>'.$msg_details.'</p>
                                         ';
                                       
                                   ?>
                                   
                              </ul>
                              <ul class="box-req box-holder clearfix" >
                                  <h3>Remarks:</h3>
                                 
                              </ul>
                          </div>
                          <ul class="command-wrap clearfix" id="command">
                              <li id="m">
                                  <input type="hidden" name="req_id" id="req_id" value="<?php echo $req_id ?>" />
                                  <input type="hidden" name="msg_id" id="msg_id" value="<?php echo $msg_id ?>" />
                                  <input type="hidden" name="msg-desc" value="<?php echo $msg_details ?>" />
                              </li>
                              <li id="g" hidden><img id="loader" src="<?php echo base_url('img/loading.gif'); ?>" width="25" height="25" /></li>
                          </ul>
                          </form>
                       </li>
                   </ul>
               </li>
           </ul>
        </section>
