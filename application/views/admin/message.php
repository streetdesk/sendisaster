        <section class="clearfix">
           <ul class="content-wrap">
               <li class="left-content">
                   <ul class="inner-wrap clearfix">
<!--
                       <li class="sms-options clearfix">
                          <?php //$this->dashboard_menu->menu(); ?>
                       </li>
-->
                       
                       <div class="heading clearfix">
                           <div class="heading-area">
                               <span>Dashboard : Messaging</span>
                           </div>
                       </div>
                       <div class="page-description">
                           <div class="page-description-area">
                               <span>This is where you send a messages</span>
                           </div>
                       </div>
                       <li class="sms-viewer clearfix">
                          <form action="<?php echo base_url('sendingprogress'); ?>" method="post" >
                              <div class="messaging-wrap clearfix">
                                  <ul class="messaging-text-wrap clearfix">
                                      <li class="messaging"><p>Description: </p>
                                           <textarea id="message" name="message-desc" class="input-text input-message clearfix" placeholder="Enter Your Message Here..."></textarea>
                                      </li>
                                  </ul>
                              </div>
                              <ul class="command-wrap clearfix" id="command">
                                  <li id="m">
                                      <input type="submit" name="send" value="Publish" class="btn" id="send-message"/>
                                      <input type="hidden" name="temp_dept_id" id="temp_dept_id" value="" />
                                      <input type="hidden" name="cat_id" id="cat_ids" value="" />
                                  </li>
                                  <li id="g" hidden><img id="loader" src="<?php echo base_url('img/loading.gif'); ?>" width="25" height="25" /></li>
                              </ul>
                          </form>
                       </li>
                   </ul>
               </li>
               <li class="right-content">
                  <ul class="notify-holder clearfix">
                        <li id="notify" class="notify-ok" hidden="hidden">
                            <ul class="notify-inner clearfix">
                                <li class="notify-desc"></li>
                                <li class="close-notify"><a href="#">x</a></li>
                            </ul>
                        </li>
                    </ul>
                  <ul class="box-holder msg-type clearfix">
                      <h3>Contacts:</h3>
                      
                      <li><a href="<?php echo base_url('addcontact') ?>">Add new contacts</a></li>
                     <li><input type="checkbox" value="0" id="ch_0"/>All</li>
                      <?php 
                          $num = 1;
                          $ad_id = $this->session->userdata('ad_id');
                          $sql = $this->db->query("SELECT * FROM department where ad_id = ".$ad_id);
                          foreach($sql->result() as $row):
                              echo '<li><input type="checkbox" value="'.$row->dept_id.'" id="ch_'.$num.'" class="check_box"/>'.$row->dept_name.'</li>';
                               ?>
                            <script type="text/javascript" src="<?php echo base_url('js/jquery.js'); ?>"></script>
                        <script>
                            $(function(){
                                $('#ch_<?php echo $num; ?>').click(function(){
                                    var category = $('#category').val(); 
                                    var message = $('#message').val();
                                    var query = 0;
                                    var temp = "";
                                     <?php $nums = 1;
                                           $sql = $this->db->query('SELECT * from department where ad_id = '.$ad_id);
                                           foreach($sql->result() as $row):     
                                                echo " if($('#ch_".$nums."').is(':checked')){
                                                temp = temp + $('#ch_".$nums."').val();
                                                }
                                                     ";  
                                           
                                                $nums++;     
                                           endforeach;
                                     ?>
                                     $('#temp_dept_id').val(temp);
                                
                                });
                            })
                        </script>  
                              
                          <?php
                              $num++;
                            endforeach; ?>
                          
                      
                      <li>Add Department:<input type="text" name="dept" id="dept"/><input type="button" name="save-dept" id="save-dept" class="btn" value="Save" /></li>
                      
                  </ul>
                  <ul class="box-holder">
<!--                      <h3>Category:</h3>-->
                      <?php /*
                         $sql = $this->db->query("SELECT * from category");
                         $num = 0;
                         $i = 1;
                         $z = "'";
                         foreach($sql->result() as $row):
//                                 echo '<option value="'.$row->cat_id.'">'.$row->cat_name.'</option>';
                                 if($num == 0):
                                     echo '<li><input type="radio" id="cat_'.$i.'" name="category" value="'.$row->cat_id.'" checked/>'.$row->cat_name.'</li>';
                                 else:
                                     echo '<li><input type="radio" id="cat_'.$i.'" name="category" value="'.$row->cat_id.'" />'.$row->cat_name.'</li>';
                                 endif;
                                 echo '
                                     <script type="text/javascript" src="'.base_url('js/jquery.js').'"></script>
                                      <script>
                                          $(function(){
                                              $('.$z.'#cat_'.$i.$z.').click(function(){
                                                  var o = $('.$z.'#cat_'.$i.$z.').val();
                                                  $('.$z.'#cat_ids'.$z.').val(o);
                                              });
                                          })
                                      </script>
                                      ';
                                 
                                 $num++;
                                 $i++;
                         endforeach;*/

                     ?>
                  </ul>
               </li>
           </ul>
        </section>
