<section class="clearfix">
           <ul class="content-wrap">
               <li class="contact contact-list">
                  <h2 class="font_reg">Contact List</h2>
                  <ul class="contact-search clearfix">
                      <li>
                          <select class="contact-dept-search" id="contact-dept-search">
                                 <option value="0" selected></option>
                                 <?php 
                                    $sql = $this->db->query('SELECT * from department');
                                    foreach($sql->result() as $row):
                                        echo '<option value="'.$row->dept_id.'">'.$row->dept_name.'</option>';
                                    endforeach;
                                 ?>   
                          </select>
                          Department Name
                      </li>
                      <li>
                          <input type="text" id="search-query"/>
                          Contact No. or Name 
                      </li>
                      <li><input type="button" name="search-contact" value="Search contact" class="btn" id="search-contact" /></li>
                      <li class="cn_loader" hidden><img src="<?php echo base_url('img/loading.gif'); ?>" alt="" class="loader-small" /></li>
                  </ul>
                 <div id="list-contact-result" class="result">
                     <?php 
                         $num = 0;
                     ?>
                     <?php
                         if($ad_id == '1'):
                             $query ="SELECT * from contacts where mun_id = 0 OR dept_id = 1 LIMIT 0, 30";
                         else:
                             $query ="SELECT * from contacts where mun_id = ".$mun_id." LIMIT 0, 30";
                         endif;
                     ?>
                     <?php  $sql = $this->db->query($query); ?>
                     <?php foreach($sql->result() as $row): ?>
                     <?php $num++; ?>
                     <div class="contact-wrap clearfix">
                       <ul class="contact-row contact-row-one clearfix">
                              <li class="contact-name"><?php echo $row->cn_name ?></li>
                              <li class="panel-holder">
                                  <ul class="panel">
                                      <script type="text/javascript" src="<?php echo base_url('js/jquery.js'); ?>"></script>
                                      <script>
                                          $(function(){
                                              $('#edit_<?php echo $num ?>').click(function(){
                                                  var cn_id = <?php echo $row->cn_id; ?>;
                                                  $.post("<?php echo base_url("addcontact/viewspecificcontact"); ?>",{cn_id:cn_id},function(result){
                                                        $('#cn_number').val(result.cn_number);
                                                        $('#cn_name').val(result.cn_name);
                                                        $('#cn_mail').val(result.cn_mail);
                                                        $('#cn_dept').prop("selectedIndex", result.dept_id);
                                                        $('#cn_brgy').prop("selectedIndex", result.brgy_id);
                                                        console.log(result.dept_id);
                                                  },"json");
                                              });
                                          })
                                          
                                          $(function(){
                                              $('#app_<?php echo $num ?>').click(function(){
                                                  var conf= confirm("Are You sure you want to Approve?");
                                                  var cn_id = "<?php echo $row->cn_id; ?>";
                                                  var cn_name = "<?php echo $row->cn_name ?>";
                                                  var cn_number = "<?php echo $row->cn_number ?>";
                                                  var dept_id = "<?php echo $row->dept_id ?>";
                                                  var cn_mun_id = "<?php echo $row->mun_id ?>";
                                                  if(conf == true){
                                                      $.post("<?php echo base_url("addcontact/approvecontact") ?>",{cn_id:cn_id,cn_name:cn_name,cn_number:cn_number,dept_id:dept_id,cn_mun_id:cn_mun_id},function(result){
                                                         console.log(result); 
                                                         if(result == 1){
                                                                alert("Contact has been Successfully added");                                                                              
                                                          }
                                                      });
                                                  }
                                              });
                                          })
                                          
                                          $(function(){
                                              $('#dapp_<?php echo $num ?>').click(function(){
                                                  var conf= confirm("Are You sure you want to Disapprove?");
                                                  var cn_id = <?php echo $row->cn_id; ?>;
                                                  var cn_name = <?php echo $row->cn_name ?>;
                                                  var cn_number = <?php echo $row->cn_number ?>;
                                                  if(conf == true){
                                                      $.post("<?php echo base_url("addcontact/disapprovecontact") ?>",{cn_id:cn_id},function(result){
                                                          if(result == 1){
                                                                alert("Contact has been Disapprove!");                                                                              
                                                          }
                                                      });
                                                  }
                                              });
                                          })
                                      </script>
                                      <?php if($row->status == 0): ?>
                                       <li><a href="#" id="app_<?php echo $num ?>">Approve</a></li>
                                      <li><a href="#" id="dapp_<?php echo $num ?>">Disapprove</a></li>
                                      <?php endif; ?>
                                      <li><a href="#" id="edit_<?php echo $num ?>">Edit</a></li>
                                      <li><a href="#">Activate/Deactivate</a></li>
                                  </ul>
                              </li>
                            </ul>
                            <ul class="contact-row contact-row-two clearfix">
                              <li>Contact No.: <?php echo $row->cn_number ?></li>
                              <?php 
                                  $sql_mun = $this->db->query("SELECT * from municipals where mun_id = ".$row->mun_id);
                                  foreach($sql_mun->result() as $rowm):
                                      echo '<li>Municipal: '.$rowm->mun_name.'</li>';
                                  endforeach;    
                              ?>
                              
                              <?php if($row->dept_id == 0): ?>
                                  <li>Department: <?php echo 'None'; ?> </li>
                              <?php else: ?>          
                                  <?php
                                      $sqls = $this->db->query("SELECT * from department where dept_id =".$row->dept_id);
                                      foreach($sqls->result() as $rows):
                                  ?>
                                      <li>Department: <?php echo $rows->dept_name ?> </li>
                                      <?php endforeach; ?>
                              <?php endif; ?>  
                          </ul>
                      </div>
                     <?php endforeach; ?>
                 </div>
               </li>
               <li class="contact contact-form clearfix">
                   
                    <style type="text/css">
                        .inputs {width:180px;}

                        div.container { border:solid 0px; padding:5px;}
                        div.container h2 {border-bottom:solid #68ba5b 1px; margin-bottom:17px;}
                        ul.input-holder { margin:5px; }                
                        ul.input-holder li {float:left;list-style:none;}
                        ul.input-holder li:nth-child(odd){ padding:5px; text-align: left; width: 106px; }
                    </style>
                <div class="container clearfix">
                <ul class="notify-holder clearfix">
                        <li id="notify" class="notify-ok" hidden="hidden">
                            <ul class="notify-inner clearfix">
                                <li class="notify-desc"></li>
                                <li class="close-notify"><a href="#">x</a></li>
                            </ul>
                        </li>
                    </ul>
                    <h2>Add New Contact:</h2>
                    <ul class="input-holder clearfix">
                        <li>Contact No.:</li>
                        <li><input type="text" id="cn_number" class="inputs input-text" onkeypress="addDashes(this)"/></li>
                    </ul>
                    <ul id="notify-cnumber" class="notify-contact input-holder clearfix" hidden>
                    </ul> 
                     <ul class="input-holder clearfix">
                        <li>Name:</li>
                        <li><input type="text" id="cn_name" class="inputs input-text"/></li>
                    </ul>
                     <ul id="notify-name" class="notify-contact input-holder clearfix" hidden>
                    </ul>
                     <ul class="input-holder clearfix">
                        <li>Mail:</li>
                        <li><input type="text" id="cn_mail" class="inputs input-text"/></li>
                    </ul>
                    <ul id="notify_email" class="notify-contact input-holder clearfix" hidden>
                    </ul>
                    <ul class="input-holder clearfix">
                        <li>Department:</li>
                        <li>
                            <select id="cn_dept" class="inputs input-text">
                                    <option value="0" selected></option>
                                    <?php 
                                        $sql = $this->db->query("SELECT * from department where ad_id = ".$this->session->userdata('ad_id'));
                                        foreach($sql->result() as $row):
                                            echo '<option value="'.$row->dept_id.'">'.$row->dept_name.'</option>';
                                        endforeach;
                                    ?>    
                            </select>
                        </li>
                    </ul>
                   <ul id="notify-dept" class="notify-contact input-holder clearfix" hidden>
                    </ul>  
                    <ul class="input-holder clearfix">
                        <li>Barangay:</li>
                        <li>
                            <select id="cn_brgy" class="inputs input-text">
                                    <option value="0" selected></option>
                                <?php 
//                                    $sql = $this->db->query("SELECT * from barangay where mun_id = ".$this->session->userdata('mun_id'));
//                                    foreach($sql->result() as $row):
//                                        echo '<option value="'.$row->brgy_id.'">'.$row->brgy_name.'</option>';
//                                    endforeach;
                                ?>
                            </select>
                        </li>
                    </ul>
                    <ul id="notify-brgy" class="notify-contact input-holder clearfix" hidden>
                    </ul>   
                    <ul class="input-holder clearfix">
                        <li></li>
                        <li class="savecon"><input type="button" name="Save_Contact" value="Save Contact" class="btn" id="save-contact" /><img src="<?php echo base_url('icons/loading.gif') ?>" id="loader-con" style="width:25px; height:25px;" hidden/></li>                
                    </ul>
                </div>
               </li>
           </ul>
</section>           