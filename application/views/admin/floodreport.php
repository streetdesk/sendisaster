 <section class="clearfix">
           <ul class="content-wrap">
               <li class="left-content">
                   <h2 class="font_reg">Dashboard</h2>
                   <ul class="inner-wrap clearfix">
                       <div class="heading clearfix">
                           <div class="heading-area">
                               <span>Report Analysis</span>
                           </div>
                       </div>
                       <div class="page-description">
                           <div class="page-description-area">
                               <span>The presented data were based from the received reports</span>
                           </div>
                       </div>
                          <div id="chart-container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
                   </ul>
               </li>
           </ul>
        </section>