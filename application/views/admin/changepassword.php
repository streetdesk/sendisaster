 <?php 
        $admin ='';        
        $sql = $this->db->query("SELECT * from administrator where ad_id = ".$ad_id);
        foreach($sql->result() as $row):
                $admin = array(
                      'ad_id'=>$row->ad_id,
                      'cn_id'=>$row->cn_id,
                      'mun_id'=>$row->mun_id,
                      'dept_id'=>$row->dept_id,
                      'ad_mail'=>$row->ad_mail,
                      'ad_username'=>$row->ad_username,
                      'ad_password'=>$row->ad_password,
                      'ad_lastname'=>$row->ad_lastname,
                      'ad_middlename'=>$row->ad_middlename,
                      'ad_firstname'=>$row->ad_firstname,
                      'ad_contact_no'=>$row->ad_contact_no,
                      'ad_address_no'=>$row->ad_address_no,
                      'ad_address_brgy'=>$row->ad_address_brgy,
                      'ad_province'=>$row->ad_province,
                      'ad_access'=>$row->ad_access
                      );      
        endforeach;
        
        ?>
        <?php 
                $municipal = "";
                $mun_ids = $admin['mun_id'];  
                
            $sqlx = $this->db->query("SELECT * from municipals where mun_id = ".$mun_ids);
            foreach($sqlx->result() as $rowx):
                    $municipal = array(
                                       'mun_id' =>$rowx->mun_id,
                                       'mun_zcode' =>$rowx->mun_zcode,
                                       'mun_name' =>$rowx->mun_name,
                                       'mun_image' =>$rowx->mun_image
                                       );
            endforeach;
        ?>
        <section class="clearfix">
           <ul class="content-wrap">
               <li class="left-content">
                   <form action="<?php echo base_url('changepassword/savepassword') ?>" method="POST">
                   <ul class="inner-wrap clearfix">
                       <div class="heading clearfix">
                           <div class="heading-area">
                               <span>Dashboard : Change Password</span>
                           </div>
                       </div>
                       <div class="page-description">
                           <div class="page-description-area">
                               <span>Change Your Password</span>
                           </div>
                       </div>
                       <li class="sms-viewer clearfix">
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Your Old Password: </p>
                                       <input type="password" id="message" name="old_pass" class="input-text-two clearfix"  value=""/>
<!--                                       <span class="notify-opass">lorem</span>-->
                                  </li>
                              </ul>
                          </div>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Your New Password: </p>
                                       <input type="password" id="message" name="new_pass" class="input-text-two clearfix"  value=""/>
<!--                                       <span class="notify-npass">lorem</span>-->
                                  </li>
                              </ul>
                          </div>
                          <div class="messaging-wrap clearfix">
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Verify Your New Password: </p>
                                       <input type="password" id="message" name="verify_pass" class="input-text-two clearfix"  value=""/>
<!--                                       <span class="notify-vpass">lorem</span>-->
                                  </li>
                              </ul>
                          </div>
                          <ul class="command-wrap clearfix" id="command">
                              <li id="m">
                                  <input type="submit" name="Save" value="Save Changes" class="btn" id="send-message"/>
                                  <input type="hidden" name="temp_dept_id" id="temp_dept_id" value="" />
                                  <input type="hidden" name="cat_id" id="cat_ids" value="" />
                              </li>
                              <li id="g" hidden><img id="loader" src="<?php echo base_url('img/loading.gif'); ?>" width="25" height="25" /></li>
                          </ul>
                       </li>
                   </ul>
                   </form>
               </li>
               <li class="right-content">
                  <div class="weather-holder clearfix">
                    <a href="http://www.accuweather.com/en/ph/pangasinan/764649/weather-forecast/764649" class="aw-widget-legal">
<!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
-->
</a><div id="awtd1428050678124" class="aw-widget-36hour"  data-locationkey="764649" data-unit="c" data-language="en-us" data-useip="false" data-uid="awtd1428050678124" data-editlocation="false"></div><script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>

                  </div>
               </li>
           </ul>
        </section>
