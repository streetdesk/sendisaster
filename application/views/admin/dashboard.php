        <?php 
        $admin = array(
                      'ad_id'=>'',
                      'cn_id'=>'',
                      'mun_id'=>'',
                      'dept_id'=>'',
                      'ad_mail'=>'',
                      'ad_username'=>'',
                      'ad_password'=>'',
                      'ad_lastname'=>'',
                      'ad_middlename'=>'',
                      'ad_firstname'=>'',
                      'ad_contact_no'=>'',
                      'ad_address_no'=>'',
                      'ad_address_brgy'=>'',
                      'ad_province'=>'',
                      'ad_access'=>''
                      );        
        $sql = $this->db->query("SELECT * from administrator where ad_id = ".$ad_id);
        foreach($sql->result() as $row):
                $admin = array(
                      'ad_id'=>$row->ad_id,
                      'cn_id'=>$row->cn_id,
                      'mun_id'=>$row->mun_id,
                      'dept_id'=>$row->dept_id,
                      'ad_mail'=>$row->ad_mail,
                      'ad_username'=>$row->ad_username,
                      'ad_password'=>$row->ad_password,
                      'ad_lastname'=>$row->ad_lastname,
                      'ad_middlename'=>$row->ad_middlename,
                      'ad_firstname'=>$row->ad_firstname,
                      'ad_contact_no'=>$row->ad_contact_no,
                      'ad_address_no'=>$row->ad_address_no,
                      'ad_address_brgy'=>$row->ad_address_brgy,
                      'ad_province'=>$row->ad_province,
                      'ad_access'=>$row->ad_access
                      );      
        endforeach;
        
        ?>
        <?php 
            
                $municipal = "";
                $mun_ids = $admin['mun_id'];  
                $municipal = array(
                                       'mun_id' =>'',
                                       'mun_zcode' =>'',
                                       'mun_name' =>'Pangasinan',
                                       'mun_image' =>'images/department/PDRRMC_pangasinan.jpg'
                                       );
            
            if(!$admin['ad_id'] == 1):
            $sqlx = $this->db->query("SELECT * from municipals where mun_id = ".$mun_ids);
            foreach($sqlx->result() as $rowx):
                    $municipal = array(
                                       'mun_id' =>$rowx->mun_id,
                                       'mun_zcode' =>$rowx->mun_zcode,
                                       'mun_name' =>$rowx->mun_name,
                                       'mun_image' =>$rowx->mun_image
                                       );
            endforeach;
            
            $municipal['mun_name'] = "Pangasinan";
            endif;
        ?>
        
        
        <section class="clearfix">
                     
           <ul class="content-wrap">
               <li class="left-content">
                   <ul class="inner-wrap clearfix">
                        <div class="heading clearfix">
                           <div class="heading-area">
                               <span>Dashboard</span>
                           </div>
                       </div>
                       <div class="page-description">
                           <div class="page-description-area">
                               <span>This is where you manage your stuff</span>
                           </div>
                       </div>
                       <?php 
                           if(isset($notif)){
                           echo '<ul class="notify-holder clearfix">
                            <li id="notify" class="notify-ok">
                                <ul class="notify-inner clearfix">
                                    <li class="notify-desc" style="  width: 97%;">'.$notif.'</li>
                                    <li class="close-notify"><a href="#">x</a></li>
                                </ul>
                            </li>
                        </ul>';
                       }unset($notif);
                       
                       ?>
                       <div class="active-list clearfix">
                           <div class="account clearfix">
                               <ul class="account-divider clearfix">
                                   <li class="account-left clearfix ">
                                       <div class="account-image ">
                                           <img src="<?php echo base_url($municipal['mun_image']) ?>" alt="" />
                                       </div>
                                   </li>
                                   <li class="account-right">
                                       <div class="account-mun">
                                           <span class="account-mun-area"><?php echo $municipal['mun_name']; ?></span>
                                       </div>
                                       <div class="account-user">
                                           <span class="account-user-area">Account User:<?php echo $admin['ad_firstname']; ?></span>
                                       </div>
                                       <div class="account-settings">
                                           <span class="account-settings-area">
                                                   <img src="<?php echo base_url('img/Users.png'); ?>" class="icon-size"  />
                                                   <a href="<?php echo base_url('profilesettings/'); ?>">Profile Settings</a>
                                            </span>
                                            <span class="account-settings-area">
                                                   <img src="<?php echo base_url('img/Passkey.png'); ?>" alt="" class="icon-size" />
                                                   <a href="<?php echo base_url('changepassword/'); ?>">Change Password</a>
                                            </span>
                                       </div>
                                   </li>
                               </ul>
                           </div>
                           <ul class="active-list-menu">
                                <li>
                                    <a href="<?php echo base_url('announcement'); ?>" class="link-menu">
                                    <ul class="inner-list-menu">
                                        <li class="list-icon-menu"><img src="<?php echo base_url('img/Announcement.png'); ?>" alt="" class="icon-size-menu" /></li>
                                        <li class="list-name-menu">
                                            <span>Announcements</span>
                                        </li>        
                                    </ul>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('message') ?>" class="link-menu">
                                    <ul class="inner-list-menu">
                                        <li class="list-icon-menu"><img src="<?php echo base_url('img/Message.png'); ?>" alt="" class="icon-size-menu" /></li>
                                        <li class="list-name-menu">
                                            <span>Message</span>
                                        </li>        
                                    </ul>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('addcontact/') ?>" class="link-menu">
                                    <ul class="inner-list-menu">
                                        <li class="list-icon-menu"><img src="<?php echo base_url('img/Phonebook.png'); ?>" alt="" class="icon-size-menu" /></li>
                                        <li class="list-name-menu">
                                            <span>Contacts</span>
                                        </li>        
                                    </ul>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('Dashboard/request/') ?>" class="link-menu">
                                    <ul class="inner-list-menu">
                                        <li class="list-icon-menu"><img src="<?php echo base_url('img/Request.png'); ?>" alt="" class="icon-size-menu" /></li>
                                        <li class="list-name-menu">
                                            <span>Request</span>
                                        </li>        
                                    </ul>
                                    </a>
                                </li>
                                
                                <?php  
                                     if($admin['ad_access'] != 1): ?>
                                    <li>
                                        <a href="<?php echo base_url('Dashboard/barangay') ?>" class="link-menu">
                                        <ul class="inner-list-menu">
                                            <li class="list-icon-menu"><img src="<?php echo base_url('img/Barangay.png'); ?>" alt="" class="icon-size-menu" /></li>
                                            <li class="list-name-menu">
                                                <span>Barangay</span>
                                            </li>        
                                        </ul>
                                        </a>
                                    </li>
                                <?php else: ?>
<!--
                                <li>
                                    <a href="#" class="link-menu">
                                    <ul class="inner-list-menu">
                                        <li class="list-icon-menu"><img src="<?php// echo base_url('img/Municipal.png'); ?>" alt="" class="icon-size-menu" /></li>
                                        <li class="list-name-menu">
                                            <span>Municipals</span>
                                        </li>        
                                    </ul>
                                    </a>
                                </li>
-->
                                <?php endif;?>

<!--
                                <li>
                                    <a href="#" class="link-menu">
                                    <ul class="inner-list-menu">
                                        <li class="list-icon-menu"><img src="<?php// echo base_url('img/Department.png'); ?>" alt="" class="icon-size-menu" /></li>
                                        <li class="list-name-menu">
                                            <span>Department</span>
                                        </li>        
                                    </ul>
                                    </a>
                                </li>
-->
                                <li>
                                    <a href="<?php echo base_url('Dashboard/floodreport'); ?>" class="link-menu">
                                    <ul class="inner-list-menu">
                                        <li class="list-icon-menu"><img src="<?php echo base_url('img/Chart.png'); ?>" alt="" class="icon-size-menu" /></li>
                                        <li class="list-name-menu">
                                            <span>Report</span>
                                        </li>        
                                    </ul>
                                    </a>
                                </li>
                           </ul>
                       </div> 
                   </ul>
               </li>
               <li class="right-content">
                        
                    <div class="weather-holder clearfix">
                    <a href="http://www.accuweather.com/en/ph/pangasinan/764649/weather-forecast/764649" class="aw-widget-legal">
<!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
-->
</a><div id="awtd1428050678124" class="aw-widget-36hour"  data-locationkey="764649" data-unit="c" data-language="en-us" data-useip="false" data-uid="awtd1428050678124" data-editlocation="false"></div><script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>

                  </div>
               </li>
           </ul>
        </section>
