  <section class="clearfix">
           <ul class="content-wrap">
               <li class="left-content">
                   <h2 class="font_reg">Dashboard</h2>
                   <ul class="inner-wrap clearfix">
                       <li class="sms-options clearfix">
                            <?php $this->dashboard_menu->menu(); ?>
                       </li>
                       <li class="sms-viewer clearfix">
                          <h3>Request</h3>
                          <ul class="request-header clearfix">
                              <li>Sender</li>
                              <li>Details</li>
                              <li>Date</li>
                              <li>Options</li>
                          </ul>
<!--
                          
                             <ul class="req-row clearfix">
                                 <li class="req-sender">09104640155</li>
                                 <li class="req-details">Lorem ipsum dolor sit amet.</li>
                                 <li class="req-date">01/01/2015</li>
                                 <li class="req-options"><a class="btn req-btn" href="#" >Respond</a></li>
                             </ul>
                             
                             
                             <ul class="req-row clearfix">
                                 <li class="req-sender"><b>09104640155</b></li>
                                 <li class="req-details"><b>Lorem ipsum dolor sit amet.</b></li>
                                 <li class="req-date"><b>01/01/2015</b></li>
                                 <li class="req-options"><a class="btn req-btn" href="#" >Respond</a></li>
                             </ul>
-->

                          
<!--                     asdasdas-->
                          <div class="request-holder clearfix">
                                   <?php if($ad_access == 2):?>
                                       <?php 
                                          $num="";
                                          $sql = $this->db->query("SELECT * from request where ad_id = ".$ad_id." ORDER by req_date ");
                                          foreach($sql->result() as $row):
                                          $msg_excerpt = substr($row->req_details, 0,40).'...';
                                          $msg_date = $row->req_date.' '.$row->req_time;
                                          $from_ad_name =  $row->mobile_number;
                                          $req_stat = $row->req_stat;
                                          $req_mark = $row->req_mark;
                                          $stat = "";
                                          if($req_stat == 0){
                                              $stat = "Pending";
                                          }else if($req_stat == 1){
                                              $stat = "Approved";
                                          }else{
                                              $stat = "Disapproved";
                                          }
                                      ?>    
                                    <?php if($req_mark == 1): ?>
                                        <ul class="req-row clearfix">
                                             <li class="req-sender"><?php echo $from_ad_name; ?></li>
                                             <li class="req-details"><?php echo $msg_excerpt ?></li>
                                             <li class="req-date"><?php echo $msg_date ?></li>
                                             <li class="req-options"><?php echo $stat; ?></li>
                                         </ul>
                                    <?php else: ?>
                                        
                                         <ul class="req-row clearfix">
                                             <li class="req-sender"><b><?php echo $from_ad_name; ?></b></li>
                                             <li class="req-details"><b><?php echo $msg_excerpt ?></b></li>
                                             <li class="req-date"><b><?php echo $msg_date ?></b></li>
                                             <li class="req-options"><a class="btn req-btn" href="<?php echo base_url('Dashboard/issuerequest?req_id='.$row->req_id) ?>" >Set Issue</a></li>
                                         </ul>
                                    <?php endif; ?>      
                                     <?php endforeach; ?>
                                   <?php elseif($ad_access == 3): ?>
                                    <?php 
                                          $num="";
                                          $sql = $this->db->query("SELECT * from messages where msg_to = ".$ad_dept_id." ORDER by msg_date ");
                                          foreach($sql->result() as $row):
                                          $msg_excerpt = substr($row->msg_details, 0,40).'...';
                                          $msg_date = $row->msg_date.' '.$row->msg_time;
                                          $sender = $row->ad_id;
                                          $msg_mark = $row->msg_mark;
                                          $msg_remarks = $row->msg_remarks;
                                      ?>    
                                          
                                          <?php 
                                               if($msg_remarks == 0){
                                                  $stat = "Pending";
                                              }else if($msg_remarks == 1){
                                                  $stat = "Approved";
                                              }else{
                                                  $stat = "Disapproved";
                                              }
                                          ?>
                                          <?php
                                              $sql_sender = $this->db->query("SELECT * FROM administrator WHERE ad_id =".$sender);
                                              foreach($sql_sender->result() as $row_sender):
                                                      $smun_id = $row_sender->mun_id;
                                                      $sdept_id = $row_sender->dept_id;
                                              endforeach;
                                          ?>
                                         <?php
                                              $sql_mun = $this->db->query("SELECT * FROM municipals WHERE mun_id =".$smun_id);
                                              foreach($sql_mun->result() as $row_mun):
                                                      $smun_name = $row_mun->mun_name;
                                              endforeach;
                                          ?>
                                          <?php
                                              $sql_dept = $this->db->query("SELECT * FROM department WHERE dept_id =".$sdept_id);
                                              foreach($sql_dept->result() as $row_dept):
                                                      $sdept_name = $row_dept->dept_name;
                                              endforeach;
                                          ?>
                                          <?php if($msg_mark == 1): ?>
                                              
                                              <ul class="req-row clearfix">
                                                 <li class="req-sender"><?php echo $smun_name.' - '.$sdept_name; ?></li>
                                                 <li class="req-details"><?php echo $msg_excerpt ?></li>
                                                 <li class="req-date"><?php echo $msg_date ?></li>
                                                 <li class="req-options"><?php echo $stat ?></li>
                                             </ul>
                                            
                                          <?php else: ?>
                                              
                                              <ul class="req-row clearfix">
                                                 <li class="req-sender"><b><?php echo $smun_name.' - '.$sdept_name; ?></b></li>
                                                 <li class="req-details"><b><?php echo $msg_excerpt ?></b></li>
                                                 <li class="req-date"><b><?php echo $msg_date ?></b></li>
                                                 <li class="req-options"><a class="btn req-btn" href="<?php echo base_url('Dashboard/respondrequest?req_id='.$row->req_id).'&msg_id='.$row->msg_id; ?>" >Respond</a>></li>
                                             </ul>
                                          <?php endif;?>
                                     <?php endforeach; ?>
                                   <?php elseif($ad_access == 4): ?>
                                       <?php 
                                          $num="";
                                          $sql = $this->db->query("SELECT * from messages where msg_to = ".$ad_dept_id." ORDER by msg_date ");
                                          foreach($sql->result() as $row):
                                          $msg_excerpt = substr($row->msg_details, 0,40).'...';
                                          $msg_date = $row->msg_date.' '.$row->msg_time;
                                          $sender = $row->ad_id;
                                          $msg_mark = $row->msg_mark;
                                          $msg_remarks = $row->msg_remarks;
                                      ?>    
                                          
                                          <?php 
                                               if($msg_remarks == 0){
                                                  $stat = "Pending";
                                              }else if($msg_remarks == 1){
                                                  $stat = "Approved";
                                              }else{
                                                  $stat = "Disapproved";
                                              }
                                          ?>
                                          <?php
                                              $sql_sender = $this->db->query("SELECT * FROM administrator WHERE ad_id =".$sender);
                                              foreach($sql_sender->result() as $row_sender):
                                                      $smun_id = $row_sender->mun_id;
                                                      $sdept_id = $row_sender->dept_id;
                                              endforeach;
                                          ?>
                                         <?php
                                              $sql_mun = $this->db->query("SELECT * FROM municipals WHERE mun_id =".$smun_id);
                                              foreach($sql_mun->result() as $row_mun):
                                                      $smun_name = $row_mun->mun_name;
                                              endforeach;
                                          ?>
                                          <?php
                                              $sql_dept = $this->db->query("SELECT * FROM department WHERE dept_id =".$sdept_id);
                                              foreach($sql_dept->result() as $row_dept):
                                                      $sdept_name = $row_dept->dept_name;
                                              endforeach;
                                          ?>
                                          <?php if($msg_mark == 1): ?>
                                              
                                              <ul class="req-row clearfix">
                                                 <li class="req-sender"><?php echo $smun_name.' - '.$sdept_name; ?></li>
                                                 <li class="req-details"><?php echo $msg_excerpt ?></li>
                                                 <li class="req-date"><?php echo $msg_date ?></li>
                                                 <li class="req-options"><?php echo $stat ?></li>
                                             </ul>
                                          <?php else: ?>
                                               <ul class="req-row clearfix">
                                                 <li class="req-sender"><b><?php echo $smun_name.' - '.$sdept_name; ?></b></li>
                                                 <li class="req-details"><b><?php echo $msg_excerpt ?></b></li>
                                                 <li class="req-date"><b><?php echo $msg_date ?></b></li>
                                                 <li class="req-options"><a class="btn req-btn" href="<?php echo base_url('Dashboard/respondrequest?req_id='.$row->req_id).'&msg_id='.$row->msg_id; ?>" >Respond</a></li>
                                             </ul>
                                          <?php endif;?>
                                     <?php endforeach; ?>
                                  <?php endif; ?>
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
<!--                          sdasdasd-->
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          </div>
                       </li>
                   </ul>
               </li>
           </ul>
        </section>
                                                   

                                    
