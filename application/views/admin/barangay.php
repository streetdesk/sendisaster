
        <section class="clearfix">
           <ul class="content-wrap">
               <li class="left-content">
                   <h2 class="font_reg">Dashboard</h2>
                   <ul class="inner-wrap clearfix">
                       <li class="sms-viewer clearfix">
                          <h3>Barangay List:</h3>
                          <div class="clearfix" id="barangay-list">
                              <?php 
                                  $mun_id = $this->session->userdata('mun_id');
                                  $sql = $this->db->query("SELECT * FROM barangay where mun_id = ".$mun_id);
                                  foreach($sql->result() as $row):
                                      echo '
                                          <ul class="row clearfix">
                                              <li><h4>'.$row->brgy_name.'</h4></li>
                                          </ul>
                                           ';
                                  endforeach;
                              ?>
                          </div>
                       </li>
                   </ul>
               </li>
               <li class="right-content">
                  <ul class="notify-holder clearfix">
                        <li id="notify" class="notify-ok" hidden="hidden">
                            <ul class="notify-inner clearfix">
                                <li class="notify-desc"></li>
                                <li class="close-notify"><a href="#">x</a></li>
                            </ul>
                        </li>
                    </ul>
                  <ul class="box-holder msg-type clearfix">
                          <h3>Add New Barangay</h3>
                          <li><input type="text" name="brgy_name" id="brgy_name" placeholder="Barangay name"/> <input type="button" name="save-brgy" id="save-brgy" class="btn" value="Save" /></li>
                  </ul>
               </li>
           </ul>
        </section>
