<?php 
    $num = 1;
    $ad_id = $this->session->userdata('ad_id');
    $req_id = $_GET['req_id'].'';
    if(empty($_GET['req_id'])){ show_404(); }       
?>    
        <section class="clearfix">
           <ul class="content-wrap">
               <li class="left-content">
                   <h2 class="font_reg">Dashboard</h2>
                   <ul class="inner-wrap clearfix">
                       
                       <li class="sms-viewer clearfix">
                          <h3>Issue Request</h3>
                          <form action="<?php echo base_url('Dashboard/sendissuerequest'); ?>" method="post">
                          <div class="messaging-wrap clearfix">
                              <ul class="box-holder clearfix" style="float:none; width:294px;" >
                                  
                              <?php $sql = $this->db->query("SELECT * FROM `request` WHERE req_id =  ".$req_id) ;
                                    foreach($sql->result() as $row):
                                        $brgy_id = $row->brgy_id;
                                        $req_id = $row->req_id;
                                        $mobile_number = $row->mobile_number;
                                        $rtype_id = $row->req_type;
                                        $req_details = $row->req_details;
                                        $time_date = $row->req_date.' '.$row->req_time;
                                        $req_stat = $row->req_stat;
                                    endforeach;
                                    $sql_req = $this->db->query("SELECT * FROM `request_type` WHERE rtype_id = ".$rtype_id);
                                        foreach($sql_req->result() as $rowq):
                                            $rtype_name = $rowq->rtype_name;
                                        endforeach;
                                    $sql_brgy = $this->db->query("SELECT * FROM `barangay` WHERE brgy_id = ".$brgy_id);
                                        foreach($sql_brgy->result() as $rowb):
                                            $brgy_name = $rowb->brgy_name;
                                            $mun_id = $rowb->mun_id;        
                                        endforeach;
                                        
                                        $sql_mun = $this->db->query("SELECT * FROM `municipals` WHERE mun_id =".$mun_id);    
                                        foreach($sql_mun->result() as $rowm):
                                            $mun_name = $rowm->mun_name;    
                                        endforeach;
                                    
                                    echo '
                                         <li><b>Sender:</b>'.$mobile_number.'</li>
                                         <li><b>Request Type:</b>'.$rtype_name.'</li>
                                         <li><b>Location:</b> Brgy. '.$brgy_name.', '.$mun_name.', Pangasinan</li>
                                         ';
                              ?>
                              </ul>
                              <ul class="messaging-text-wrap clearfix">
                                  <li class="messaging"><p>Description: </p>
                                       <textarea id="message" name="message-desc" class="input-text input-message clearfix" placeholder="Enter Your Message Here..."></textarea>
                                  </li>
                              </ul>
                          </div>
                          <ul class="command-wrap clearfix" id="command">
                              <li id="m">
                                  <input type="submit" name="send_req" value="Send Request" class="btn" id="send-request"/>
                                  <input type="hidden" name="req_id" id="req_id" value="<?php echo $req_id ?>" />
                                  <input type="hidden" name="temp_dept_id" id="temp_dept_id" value="" />
                                  <input type="hidden" name="cat_id" id="cat_ids" value="" />
                              </li>
                              <li id="g" hidden><img id="loader" src="<?php echo base_url('img/loading.gif'); ?>" width="25" height="25" /></li>
                          </ul>
                          </form>
                       </li>
                   </ul>
               </li>
               <li class="right-content">
                  <div class="weather-holder clearfix">
                    <a href="http://www.accuweather.com/en/ph/pangasinan/764649/weather-forecast/764649" class="aw-widget-legal">
<!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
-->
</a><div id="awtd1428050678124" class="aw-widget-36hour"  data-locationkey="764649" data-unit="c" data-language="en-us" data-useip="false" data-uid="awtd1428050678124" data-editlocation="false"></div><script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>

                  </div>
               </li>
           </ul>
        </section>
