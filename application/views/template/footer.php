<?php 
                $query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
                $my_page =  $this->uri->uri_string(). $query; 

                ?>
        <footer class="clearfix">
<!--
            <div class="author">
                <p>Site By: Jeimzone<?php //echo $my_page ?></p>
                <p>Disaster Management Via - SMS Information Dissemination System</p>
            </div>
            <ul class="dept_logos clearfix">
            <li><img src="" alt=""></li>
            <li><img src="" alt=""></li>
            <li><img src="" alt=""></li>
            <li><img src="" alt=""></li>
        </ul>
-->
        
        <ul class="footer-divider clearfix">
            <li class="author clearfix">
                <p>Site By: Jeimzone<?php echo $my_page ?></p>
                <p>Disaster Management Via - SMS Information Dissemination System</p>
            </li>
            <li class="dept_logos clearfix">
                <ul class="dept_logo clearfix">
                    <li><img src="<?php echo base_url('images/department/PDRRMC_pangasinan.jpg') ?>" alt=""></li>
                    <li><img src="<?php echo base_url('images/department/Pagasa_logo.png') ?>" alt=""></li>
                    <li><img src="<?php echo base_url('images/department/DENR_logo.png') ?>" alt=""></li>
                    <li><img src="<?php echo base_url('images/department/phivolcs-logo.jpg') ?>" alt=""></li>
                    <li><img src="<?php echo base_url('images/department/ocd.png') ?>" alt=""></li>
                </ul>
            </li>
        </ul>
        </footer>
    </body>
    
<script type="text/javascript" src="<?php echo base_url('js/jquery.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jscript.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/dashscript.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/modernizr-latest.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/html5shiv/src/html5shiv.js'); ?>"></script>
<?php if($my_page == 'Dashboard'): ?>
            <script type="text/javascript" src="<?php echo base_url('js/jquery-1.10.2.min.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url('js/jquery.tokeninput.js'); ?>"></script>
             <script type="text/javascript">
                $(function(){
                  $('#input-recepient').tokenInput([
                      {id: 1, name: "All"},
                      {id: 2, name: "BDRRMC"},
                      {id: 3, name: "Health Units"},
                      {id: 4, name: "Education Units"}
                    ], { 
                      theme: "facebook",
                      hintText: "Type Contacts",
                      noResultsText: "Nothin' found.",
                      searchingText: "Loading...",
                      preventDuplicates: true
                  }); 

                });
            </script>
<?php elseif($my_page == 'Dashboard/floodreport'): ?>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('js/highcharts.js'); ?>"></script>  
        <script type="text/javascript" src="<?php echo base_url('js/exporting.js'); ?>"></script>
        <script type="text/javascript">
$(function () {
    $('#chart-container').highcharts({
        chart: {
            renderTo: 'container',
            type: 'column',
            width: 1030
        },
        title: {
            text: 'Report Analysis'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            <?php $sqlx = $this->db->query("SELECT * FROM municipals");
                $cat = "";
                $num_row = $sqlx->num_rows();
                $num_row;
                $num = 0;
                foreach($sqlx->result() as $row):
                    if($num  == $num_row-1){
                        $cat .= "'".$row->mun_name."'";
                    }else{
                        $cat .= "'".$row->mun_name."',";
                    }
                $num++;
                endforeach;
            ?>
            <?php
                
            ?>
            categories: [<?php echo $cat ?>],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Reports',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Food',
            data: [107, 31, 635, 203, 2,107, 31, 635, 203, 6,107, 31, 635, 203, 2,107, 31, 635, 203, 10]
        }, {
            name: 'Medicine',
            data: [133, 156, 947, 408, 6, 133, 156, 947, 408, 6,133, 156, 947, 408, 6,133, 156, 947, 408, 6]
        }, {
            name: 'Rescue',
            data: [973, 914, 4054, 732, 34,973, 914, 4054, 732, 34,973, 914, 4054, 732, 34,973, 914, 4054, 732, 34]
        }, {
            name: 'Evacuation',
            data: [973, 914, 300, 732, 34,973, 914, 300, 732, 34,973, 914, 300, 732, 34,973, 914, 300, 732, 34]
        }, {
            name: 'Construction',
            data: [973, 914, 4054, 732, 34,973, 914, 4054, 732, 34,973, 914, 4054, 732, 34,973, 914, 4054, 732, 34]
        }]
    });
});
		</script>
<?php endif; ?>
</html>