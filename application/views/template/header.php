<?php $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Pragma: no-cache");  ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
           <?php
                /* URL Helper */
                $this->load->helper('url');

                /* Get Homepage or Not */
                $page = $this->uri->segment(1);
                $home = false;
                
                if ($page == 'home' || $page == '') {
                    $home = true;
                }
            ?>
              <?php
                $query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
                $my_page =  $this->uri->uri_string(). $query; 

                ?>
                <?php $ad_id = $this->session->userdata('ad_id').'';

                     if($my_page == '' || $my_page == 'preparedness' || $my_page == 'weather' || $my_page == 'Reportflood'):
                         if($ad_id != ""):
                            redirect(base_url('Dashboard'));
                        endif;
                        
                     else:
                        if($ad_id == ""):
                            $def_page = ''.base_url();
                            redirect($def_page);
                        endif;
                        
                     endif;
                ?>    
        <!--META-->
            <meta charset="utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="" />
<!--            <meta name="viewport" content="width=device-width, initial-scale=1" />-->
            <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <!--META-->
        
        <!--  Fonts  -->
            <link rel="stylesheet" type="text/css" href="<?php echo base_url('font/droidsans/stylesheet.css'); ?>" />    
        <!--  Fonts  -->
        
        <!--    Stylesheet    -->
            <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('css/style.css'); ?>" />
            <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('css/broiler.css'); ?>" />
            <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('css/normalize.css'); ?>" />
        <?php if($my_page == 'Dashboard'): ?>
            
            <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('css/token-input.css'); ?>" />
            <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('css/token-input-facebook.css'); ?>" />
        <?php endif; ?>
        <!--    Stylesheet    -->
        
        <link href="<?php echo base_url('css/facebox.css') ?>" media="screen" rel="stylesheet" type="text/css" />
        
        
        <title>iSendDisaster - ID Sys</title>
    </head>
    <body>
        <header class="gradient clearfix">
            <div class="wrapper clearfix">
                <div class="banner clearfix">
                    <h1 class="">iSenDisaster</h1>
                </div>
                <div class="right-header clearfix">
                    <nav class="clearfix">
                        <ul class="clearfix">
                            <?php if($my_page == '' || $my_page == 'preparedness' || $my_page == 'weather' || $my_page == 'Reportflood'): ?>
                                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                <li><a href="<?php echo base_url('preparedness'); ?>">Preparedness</a></li>
                                <li><a href="<?php echo base_url('weather'); ?>">Weather Forecast</a></li>
                                <li><a href="<?php echo base_url('Reportflood'); ?>">Hazard Maps</a></li>
                            <?php else: ?>
                                 <li><a href="<?php echo base_url('Dashboard/'); ?>">Dashboard</a></li>
<!--                                    <li><a href="#">Profile Settings</a></li>-->
                                <li><a href="<?php echo base_url('home/logoutuser'); ?>">Logout</a></li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
            </div>
            
        </header>