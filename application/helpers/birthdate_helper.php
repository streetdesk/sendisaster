<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('buildDayDropdown'))
{
    function buildDayDropdown($name='',$value='')
    {
        $cday = date('d');
        echo'<select class="input-ddown" id="day" name="day">';
        echo'<option value="0">Day</option>';    
            for($i = 1;$i<=31;$i++):
                if($i == $cday):
                    echo '<option value="'.$i.'" selected>'.$i.'</option>';
                else:
                    echo '<option value="'.$i.'">'.$i.'</option>';
                endif;
            endfor;
        echo '</select>';
        
    }
}	

if ( !function_exists('buildYearDropdown'))
{
	function buildYearDropdown($name='',$value='')
    {        
        $cyear = date('Y');
        echo'<select class="input-ddown" id="year" name="year">';
        echo'<option value="0">Year</option>';    
            for($i = 1992;$i<=$cyear;$i++):
                if($i == $cyear):
                    echo '<option value="'.$i.'" selected>'.$i.'</option>';
                else:
                    echo '<option value="'.$i.'">'.$i.'</option>';
                endif;
            endfor;
        echo '</select>';
    }
}

if (!function_exists('buildMonthDropdown'))
{
    function buildMonthDropdown($name='',$value='')
    {
       $cmonth = date('m');
        $month=array(
			''	=>'Month',
            '1'=>'January',
            '2'=>'February',
            '3'=>'March',
            '4'=>'April',
            '5'=>'May',
            '6'=>'June',
            '7'=>'July',
            '8'=>'August',
            '9'=>'September',
            '10'=>'October',
            '11'=>'November',
            '12'=>'December');
            
             echo'<select class="input-ddown" id="month" name="month">';
        echo'<option value="0">Month</option>';    
            for($i = 1;$i<=12;$i++):
                if($i == $cmonth):
                    echo '<option value="'.$i.'" selected>'.$month[$i].'</option>';
                else:
                    echo '<option value="'.$i.'">'.$month[$i].'</option>';
                endif;
            endfor;
        echo '</select>';
        
    }
}