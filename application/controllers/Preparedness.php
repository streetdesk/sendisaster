<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Preparedness extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $this->load->view('template/header');
		$this->load->view('preparedness');
		$this->load->view('template/footer');
	}
	function loadvideo(){
	    $frame = $_POST['frame'];
	    $src="";
	    if($frame == 1){
	        $src = "//www.youtube.com/embed/A96f2B6pB-A?rel=0";
        }else if($frame == 2){
	        $src = "//www.youtube.com/embed/dvuGyLZh-og?rel=0";
        }else{
	        $src = "//www.youtube.com/embed/eSq6_rX_kOc?rel=0";
        }
	    
	    echo $src;
	}
	
	function loadpdf(){
	    $frame = $_POST['frame'];
	    $src="";
	    if($frame == 1){
	        $src = "https://safesteps.com/wp-content/uploads/2014/04/01_TYPHOON_CARD.pdf";
        }else if($frame == 2){
	        $src = "https://safesteps.com/wp-content/uploads/2014/04/03_FLOOD_CARD.pdf";
        }else{
	        $src = "http://safesteps.com/wp-content/uploads/2014/04/05_EMERGENCY_KIT_CARD.pdf";
        }
	    
	    echo $src;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */