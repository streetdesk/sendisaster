<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addcontact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		 $var['ad_id'] = $this->session->userdata('ad_id');
		 $var['mun_id'] = $this->session->userdata('mun_id');
		 $this->load->view('template/header',$var);
		$this->load->view('admin/addcontact',$var);
		 $this->load->view('template/footer',$var);

	}
	function addnewcontact(){
	   $post = array($_POST['cn_number'],$_POST['cn_name'],$_POST['cn_mail'],$_POST['cn_dept'],$_POST['cn_brgy']);
	   
	   
	   $data = array(
	                'ad_id' => '1',
	                'cn_id' => 'NULL',
	                'cn_name' => $post[1],
	                'cn_mail'  => $post[2],
	                'cn_number' => $post[0],
	                'dept_id' => $post[3],
	                'ctype_id' =>'2',
	                'brgy_id' => $post[4]
	                 );
	    
	    $this->db->insert('contacts',$data);
	    echo '1';
	
	}
	function querycontact(){
	    $dept_id = $_POST['contact_dept_search'];
	    $search_query = $_POST['search_query'];
        $searchtype = '';
           if($dept_id == 0){
	            $searchtype = " where cn_number LIKE '%".$search_query."%' or cn_name LIKE '%".$search_query."%'";
	       }else{
	            $searchtype = " where dept_id = ".$dept_id." AND (cn_number LIKE '%".$search_query."%' or cn_name LIKE '%".$search_query."%')";
	       }
             $sql = $this->db->query("SELECT * FROM contacts".$searchtype); 
             $num_row = $sql->num_rows();
	     if($num_row != 0):
	         foreach($sql->result() as $row):
	             $name = $row->cn_name;
	             $number = $row->cn_number;
	             $mail = $row->cn_mail;
	             $dept_ids = $row->dept_id;
	            
	             $dept_name = 'None';
                 if($dept_ids != 0):
	                 $dept_query = $this->db->query("SELECT * from department where dept_id =".$dept_ids);
	                 foreach($dept_query->result() as $rows):
	                     $dept_name = $rows->dept_name;
	                 endforeach;
	             endif;
	            echo '
	                 <div class="contact-wrap clearfix">
	                 <ul class="contact-row contact-row-one clearfix">
                              <li class="contact-name">'.$name.'</li>
                              <li class="panel-holder">
                                  <ul class="panel">
                                      <li><a href="#">Edit</a></li>
                                      <li><a href="#">Activate/Deactivate</a></li>
                                  </ul>
                              </li>
                            </ul>
                            <ul class="contact-row contact-row-two clearfix">
                              <li>Contact No.: '.$number.'</li>
                              <li>E-mail: '.$mail.'</li>
                                  <li>Department: '.$dept_name.'</li>
                              <?php endif; ?>  
                          </ul>
                        </div>
	                 ';
	         endforeach;
	     else:
	         echo 'NULL';
	     endif;
	}
	function viewspecificcontact(){
        $cn_id = $_POST['cn_id'];        
        $sql = $this->db->query("SELECT * FROM contacts where cn_id = ".$cn_id);
        $num_rows = $sql->num_rows();
        if($num_rows != 0):
            foreach($sql->result() as $row):
                $args = array(
                        'cn_id'   => $row->cn_id,
                        'cn_name' => $row->cn_name,
                        'cn_number' => $row->cn_number,
                        'cn_mail' => $row->cn_mail,
                        'dept_id' => $row->dept_id,
                        'ctype_id'=> $row->ctype_id,
                        'brgy_id' => $row->brgy_id
                        );
                 echo json_encode($args);
            endforeach;
        endif;
	}
	function approvecontact(){
	    
	    $cn_id = $_POST['cn_id'];
	    $cn_name = $_POST['cn_name'];
	    $cn_number = $_POST['cn_number'];
	    $dept_id = $_POST['dept_id'];
	    $cn_mun_id = $_POST['cn_mun_id'];
	    $ad_access = $this->session->userdata('ad_access');
	    if($ad_access == 2){
	       $cn_ad_access = 4;
	    }else{
	        if($dept_id == '1'){
	            $cn_ad_access = 2;
	        }else{
	            $cn_ad_access = 3;
	        }
	    }
	    
	    $user = md5($cn_name);
	    $pass = md5($cn_number);
	    
	    $data = array(
	                 'status' => 1
	                 );
	    
	    $this->db->update('contacts', $data, "cn_id =".$cn_id);
	       
	    $data = array(
            'ad_id' =>NULL,
            'cn_id' =>$cn_id,
            'mun_id'=>$cn_mun_id,
            'dept_id'=> $dept_id,
            'ad_mail' =>'',
            'ad_username' =>$user,
            'ad_password' =>$pass,
            'ad_lastname' =>'',
            'ad_middlename' =>'',
            'ad_firstname' =>$cn_name,
            'ad_contact_no' =>$cn_number,
            'ad_address_no' =>'',
            'ad_address_brgy' =>'',
            'ad_province' =>'',
            'ad_access' =>$cn_ad_access,
             );
        
            $this->db->insert("administrator", $data);     
             
        require APPPATH . '../chikka-master/src/Chikka.php';
              $credentials = array(
                'client_id' => '33313f83a8b19ab72d260a35204e625e3d51b323edca8fa4c49f5a358acf361c',
                'secret_key'=> '1dd6823f661b8b494197feb92b2f167c35092d28efbc122212858d0eb80eb249',
                'shortcode' => '2929083668'
              );
              $chikka = new Chikka($credentials);
              $mobileNumber = $cn_number;
              $msg = 'iSendisaster: You are now Registered. Your Username is '.$cn_name.' and Your Password is '.$cn_number;

              $send = $chikka->send($mobileNumber, $msg);
              $messageId = $send->msg->message_id;
              if ($send->success()) {
                echo '1';
              } else {
                echo '0';
              }
	}
}