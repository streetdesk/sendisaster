<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profilesettings extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $var['ad_id'] = $this->session->userdata('ad_id');
        $var['ad_access'] = $this->session->userdata('ad_access');
        $var['ad_dept_id'] = $this->session->userdata('ad_dept_id');
        $this->load->view('template/header',$var);
		$this->load->view('admin/profilesettings',$var);
		$this->load->view('template/footer',$var);
	}
	function saveprofile()
    {
        
        $this->load->helper('url');
        
        $ad_id = $this->session->userdata('ad_id');
        
        $data = array(
                      'ad_lastname'=>$_POST['lname'],
                      'ad_middlename'=>$_POST['mname'],
                      'ad_firstname'=>$_POST['fname'],
                      'ad_contact_no'=>$_POST['cnumber'],
                      'ad_address_no'=>$_POST['addno'],
                      'ad_address_brgy'=>$_POST['brgy'],
                      'ad_province'=>$_POST['province']
                      ); 
        

        $this->db->where('ad_id', $ad_id);
        $this->db->update('administrator', $data); 
        
        
        header('Location: '.base_url('Dashboard/'));
    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */