<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receivemessage extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
       require APPPATH . '../chikka-master/src/Chikka.php';
      
      $credentials = array(
        'client_id' => 'aa47a03362b8d5e29643eeaf1f9594939da0023ce9c6b5b4a3f8c186fe021e48',
        'secret_key'=> '1032d03eaa5cdd125dcb140e287395465bd5f04d45854c48b8fdc0aff3193d1b',
        'shortcode' => '29290255'
      );
      $chikka = new Chikka($credentials);
      $chikka->receiveMessage(function($message) {
         $string = $message->message;
         $j = explode(" ", $string);
        
        $l = 1;
        $key = "";
        $title = "";
        $municipal = "";
        $barangay = "";
        $req_details = "";
        $req_type = "";
        $rtype_id = "";
        
        $ad_id = "";
        $mun_id = "";
        $brgy_id = "";
        $req_id = "";
        
        
        $key = $j[1];
        $date = date('m/d/Y');
        $time = date("h:i:s A");
        
        
        if($key == "SUBSCRIBE"){ //ISEND KEY MUNICIPAL
                for($i = 0 ; $i < sizeof($j) ; $i++):
                        if($l == 1):
                            $title = $j[$i];
                            $l++;
                        elseif($l == 2):
                            $key = $j[$i];
                            $l++;
                        elseif($l == 3):
                            $municipal = $j[$i];
                        endif;
                     endfor;

                     $sql = $this->db->query("SELECT * from municipals where mun_name LIKE '%".$municipal."%'");
                     foreach($sql->result() as $row):
                         $mun_id = $row->mun_id;
                     endforeach;

                     $data = array(
                                 'mun_id' =>$mun_id,
                                 'sub_id' =>NULL,
                                 'sub_number' =>$message->mobile_number,
                                 'sub_active' =>1
                                 );
                     $this->db->insert('subscriber', $data); 
                     $reply = "iSendisaster: You are Now registered. you can now receive any updates From LGU Pangasinan. Thank you";
        }
        if($key == "REQ"){
                for($i = 0 ; $i < sizeof($j) ; $i++):        //ISEND KEY REQ_TYPE MUNICIPAL DETAILS
                    if($l == 1):                               //ISEND REQ EVAC CALASIAO NALSIAN DETAILS
                        $title = $j[$i];
                        $l++;
                    elseif($l == 2):
                        $key = $j[$i];
                        $l++;
                    elseif($l == 3):
                        $req_type = $j[$i];
                        $l++;
                    elseif($l == 4):
                        $municipal = $j[$i];
                        $l++;
                    elseif($l==5):
                        $barangay = $j[$i];
                        $l++;    
                    else:
                        $req_details = $req_details . $j[$i];
                    endif;
                 endfor;
                     $sql = $this->db->query("SELECT * FROM municipals where mun_name LIKE '%".$municipal."%'");
                      foreach($sql->result() as $row):
                         $mun_id = $row->mun_id;
                      endforeach;
            
                 $sqlx = $this->db->query("SELECT * FROM barangay where brgy_name LIKE '%".$barangay."%' AND mun_id = ".$mun_id);
                 foreach($sqlx->result() as $rowx):
                     $brgy_id = $rowx->brgy_id;
                 endforeach;
             
                     $sqlz = $this->db->query("SELECT * FROM administrator where mun_id = ".$mun_id);
                     foreach($sqlz->result() as $rowz):
                         $ad_id = $rowz->ad_id;
                     endforeach;
             
                 $sqlc = $this->db->query("SELECT * from request_type where req_acyr LIKE '%".$req_type."%'");
                 foreach($sqlc->result() as $rowc):
                     $rtype_id = $rowc->rtype_id;
                 endforeach;
             
             $data = array(
                         'ad_id' => $ad_id,
                         'brgy_id' =>$brgy_id ,
                         'req_id' => NULL,
                         'req_type' => $rtype_id,
                         'mobile_number' => $message->mobile_number,
                         'req_details' => $req_details,
                         'req_time' => $time ,
                         'req_date' => $date ,  
                          'req_stat' =>0 ,
                          'req_mark' =>0 
                          );
                          
            $this->db->insert('request',$data);
             $reply = "iSendisaster: Your request has been Issued. Thank you";
        }
                                                
      $message->reply($reply, 0);
      return true;
      });
	}
}