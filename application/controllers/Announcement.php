<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcement extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	
	public function index()
	{
        $this->load->library('dashboard_menu');
        $this->load->view('template/header');
		$this->load->view('admin/announcement');
		$this->load->view('template/footer');
	}
	function do_upload()
	{  
		 $this->load->helper('url');
        $date = date("mm/dd/yyyy");
        $time = date("h:i:s A");
		$ad_id = $this->session->userdata('ad_id');
		$post_title = $_POST['post_title'];
		$post_desc = $_POST['post_description'];
		$post_date = $date.' '.$time;
		$cat_id = '';
		
		
		$images = $_FILES['images']['name'];
    	$type = $_FILES['images']['type'];
		
		$TARGET_PATH = base_url('images/post/');
	//	$path = "modules/".$cur_id.'/';
		if (!file_exists($TARGET_PATH)) {
			mkdir($TARGET_PATH, 0777, true);
		}
	 $TARGET_PATH;
		 $TARGET_PATH .= $images;
//		 $path .= $images;

			if($type == "image/jpeg" || $type == "image/png" || $type == "image/gif" || $type == "image/x-ms-bmp" ){
				 
				 if(move_uploaded_file($_FILES['images']['tmp_name'], $TARGET_PATH)){
					
					$data = array(
                                    'ad_id' =>$ad_id,
                                    'post_id'=>NULL,
                                    'post_title' =>$post_title,
                                    'post_description'=>$post_desc,
                                    'post_date'=> $post_date,
                                    'cat_id'=> $cat_id,
                                    'post_fet_img'=>$TARGET_PATH
                                 );
					$this->db->insert('post',$data);
					
					//header("Location: ".base_url('announcement'));
				}else{
					$_SESSION['error'] = "Could not upload file.  Check read/write persmissions on the directory";
				}
			}else{
					$_SESSION['error'] = "Could not upload file. Please Upload a jpg, png, bmp";
					
				}
	}
	
	function savepost(){
	    $this->load->helper('url');
        $date = date("mm/dd/yyyy");
        $time = date("h:i:s A");
		$ad_id = $this->session->userdata('ad_id');
		$post_title = $_POST['post_title'];
		$post_desc = $_POST['post_description'];
		$post_date = $date.' '.$time;
		$cat_id = '';
	    $TARGET_PATH = "";
	    
	    $data = array(
                                    'ad_id' =>$ad_id,
                                    'post_id'=>NULL,
                                    'post_title' =>$post_title,
                                    'post_description'=>$post_desc,
                                    'post_date'=> $post_date,
                                    'cat_id'=> $cat_id,
                                    'post_fet_img'=>$TARGET_PATH
                                 );
					$this->db->insert('post',$data);
	header("Location:".base_url('announcement'));
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */