<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $this->load->view('template/header');
        $this->load->library('posting');
		$this->load->view('home');
		$this->load->view('template/footer');
	}
	
	function loguser()
    {
        $this->load->helper('url');
        if(!isset($_POST['username']) || !isset($_POST['password'])){
            header('location:'.base_url('index.html'));
        }
        $ad_id = "";
        $mun_id = "";
        $cn_id = "";
        $dept_id = "";
        $user = $_POST['username'];
        $pass = $_POST['password']; 
        
        $log_result=0;
        
        $sql = $this->db->query("SELECT * FROM administrator where (ad_username ='".md5($user)."' OR ad_mail = '".$user."') AND ad_password = '".md5($pass)."'");
        $num_row = $sql->num_rows();
        
        if($num_row != 0):
            $sql = $this->db->query("SELECT * FROM administrator where (ad_username ='".md5($user)."' OR ad_mail = '".$user."') AND ad_password = '".md5($pass)."'");
            foreach($sql->result() as $row):
               $log_result = 1;
              $ad_id = $row->ad_id; 
              $mun_id = $row->mun_id;
              $dept_id = $row->dept_id;
              $cn_id = $row->cn_id;
              $ad_access  = $row->ad_access;
            endforeach; 
        endif;
             
        $newdata = array(
                   'ad_id'  => $ad_id,
                   'mun_id'  => $mun_id,
                   'ad_dept_id' => $dept_id,
                   'cn_id' => $cn_id,
                   'ad_access' => $ad_access,
                   'logged_in' => TRUE
               );

$this->session->set_userdata($newdata);
        
        echo $log_result;
        
	}
	
	function logoutuser()
    {
        $this->load->helper('url');
        $user_data = $this->session->all_userdata();

        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        } 
       header('Location: '.base_url());
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */