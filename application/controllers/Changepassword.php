<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Changepassword extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	
	public function index()
	{
        $var['ad_id'] = $this->session->userdata('ad_id');
        $var['ad_access'] = $this->session->userdata('ad_access');
        $var['ad_dept_id'] = $this->session->userdata('ad_dept_id');
        $this->load->library('dashboard_menu',$var);
        $this->load->view('template/header',$var);
		$this->load->view('admin/changepassword',$var);
		$this->load->view('template/footer',$var);
	}
	
	function savepassword()
    {
        
        $ad_id = $this->session->userdata('ad_id');
        $old_pass = md5($_POST['old_pass']);
        $new_pass = $_POST['new_pass'];
        $verify_pass = $_POST['verify_pass'];
        $exist_pass = "";
        $notif = "";
        
        $sql = $this->db->query("SELECT * from administrator where ad_id = ".$ad_id);
        foreach($sql->result() as $row):
        
              $exist_pass = $row->ad_password;
        
        endforeach;
           
       if($old_pass == $exist_pass){
           if($new_pass == $verify_pass){
             $data = array(
                      'ad_password'=>md5($new_pass)
                      );
                      
        $this->db->where('ad_id', $ad_id);
        $this->db->update('administrator', $data); 
           $notif = "Your Have successfully Change your password.";  
           }else{
               $notif = "New Password Mismatch";   
           } 
        }else{
                $notif = "Incorrect Password.";
        }
        
        
                $var['ad_id'] = $this->session->userdata('ad_id');
                $var['ad_access'] = $this->session->userdata('ad_access');
                $var['ad_dept_id'] = $this->session->userdata('ad_dept_id');
                $var['notif'] = $notif;
                $this->load->library('dashboard_menu', $var);
                $this->load->view('template/header', $var);
                $this->load->view('admin/dashboard', $var);
                $this->load->view('template/footer', $var);    
        
    }
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */