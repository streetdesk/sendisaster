<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $var['ad_id'] = $this->session->userdata('ad_id');
        $var['ad_access'] = $this->session->userdata('ad_access');
        $var['ad_dept_id'] = $this->session->userdata('ad_dept_id');
        $this->load->library('dashboard_menu', $var);
        $this->load->view('template/header', $var);
		$this->load->view('admin/dashboard', $var);
		$this->load->view('template/footer', $var);
	}
	function postmessage()    
    {
	    $category = $_POST['category'];
	    $message = $_POST['message'];
	    $date = date('m/d/Y');
	    
	    $data = array(
	                'ad_id' => '1',
	                'post_id' => 'NULL',
	                'post_description' => $message,
	                'post_date'  => $date,
	                'cat_id' => $category
	                 );
	    
	    $this->db->insert('post',$data);
	}
	function adddepartment()
    {
        $ad_id = $this->session->userdata('ad_id');
        $mun_id = $this->session->userdata('mun_id');
            $dept = $_POST['dept'];
            if($dept == ""):
                echo 0;
            else:
                $data = array(
                             'dept_id'=>NULL,
                             'ad_id' => $ad_id,
                             'mun_id' =>$mun_id,
                             'dept_name' => $dept
                             );

                $this->db->insert('department',$data);

                echo 1;
            endif;
    }
    function displaydepartment()
        {
          $ad_id = $this->session->userdata('ad_id');
          $sql = $this->db->query("SELECT * FROM department where ad_id = ".$ad_id);
          foreach($sql->result() as $row):
              echo '
                  <ul class="row clearfix">
                      <li><h4>'.$row->dept_name.'</h4></li>
                  </ul>
                   ';
          endforeach;
        } 
    function request()
    {
        $var['ad_id'] = $this->session->userdata('ad_id');
        $var['ad_access'] = $this->session->userdata('ad_access');
        $var['ad_dept_id'] = $this->session->userdata('ad_dept_id');
        $this->load->library('dashboard_menu',$var);
        $this->load->view('template/header',$var);
		$this->load->view('admin/request',$var);
		$this->load->view('template/footer',$var);
    }
    function issuerequest()
    {
        $var['ad_id'] = $this->session->userdata('ad_id');
        $var['ad_access'] = $this->session->userdata('ad_access');
        $var['ad_dept_id'] = $this->session->userdata('ad_dept_id');
        $this->load->library('dashboard_menu',$var);
        $this->load->view('template/header',$var);
		$this->load->view('admin/issuerequest',$var);
		$this->load->view('template/footer',$var);
    }
    function respondrequest()
    {
        $var['ad_id'] = $this->session->userdata('ad_id');
        $var['ad_access'] = $this->session->userdata('ad_access');
        $var['ad_dept_id'] = $this->session->userdata('ad_dept_id');
        $this->load->library('dashboard_menu',$var);
        $this->load->view('template/header',$var);
		$this->load->view('admin/respondrequest',$var);
		$this->load->view('template/footer',$var);
    }
        
    function sendissuerequest()
    {
        $this->load->helper('url');
        $ad_id = $this->session->userdata('ad_id');
        $req_id = $_POST['req_id'];
        $temp_dept_id = $_POST['temp_dept_id'].'';
        $message = $_POST['message-desc'];
        $date = date("m/d/Y");
        $time = date("h:i:s A");

     $dept_ids = explode('_',$temp_dept_id);
        for($i = 0; $i < sizeof($dept_ids)-1 ; $i++):
            if($dept_ids != ""){
                $data = array(
                 'ad_id' => $ad_id ,
                 'req_id' => $req_id ,
                 'msg_id' =>NULL ,
                 'msg_to' =>$dept_ids[$i],
                 'msg_details' =>$message,
                 'msg_date' =>$date ,
                 'msg_time' =>$time
                 );
            $this->db->insert("messages", $data);
            $id = "req_id =".$req_id;
            $data = array('req_mark' => 1);        
            
            $this->db->update('request',$data,$id);
            }
        endfor;
            header("Location: ".base_url('Dashboard/request'));

    }
    function approverequest()
    {
        $this->load->helper('url');
        $ad_access = $this->session->userdata('ad_access');
      echo  $ad_dept_id = $this->session->userdata('ad_dept_id');
        $adp = $_POST['adp'];
        if($adp == 1 || $adp == 2){
             $req_id = "req_id =".$_POST['req_id'];
             $data = array('req_stat' => 1);
             $this->db->update('request', $data, $req_id);

             $req_id = "req_id =".$_POST['req_id'];
             $data = array('msg_mark' => 1, 'msg_remarks' => $adp);
             $this->db->update('messages', $data, $req_id);
        }else{
             $ad_id = $this->session->userdata('ad_id');
             $req_id = $_POST['req_id'];   
             $message = $_POST['msg-desc'];
             $date = date("m/d/Y");
             $time = date("h:i:s A");
             $pdept = "";
             $dept_name="";
             $sqlc = $this->db->query("SELECT * FROM department where dept_id = ".$ad_dept_id);
             foreach($sqlc->result() as $rowc):
                 $dept_name = $rowc->dept_name;
             endforeach;
             if($dept_name == 'PNP'){
                 $pdept = 'PPNP';
             }else if($dept_name=='MHO'){
                 $pdept = 'PHO';
             }else if($dept_name=='MSWD'){
                 $pdept = 'DSWD';
             }else if($dept_name=='MAGO'){
                 $pdept = 'PAGO';
             }else if($dept_name=='MIO'){
                 $pdept = 'PIO';
             }else if($dept_name=='MEO'){
                 $pdept = 'PEO';
             }
             
             $sqlx = $this->db->query("SELECT * FROM department where dept_name ='".$pdept."'");
             foreach($sqlx->result() as $rowx):
            echo     $dept_ids = $rowx->dept_id;
             endforeach;
             
             $data = array(
                 'ad_id' => $ad_id ,
                 'req_id' => $req_id ,
                 'msg_id' =>NULL ,
                 'msg_to' =>$dept_ids,
                 'msg_details' =>$message,
                 'msg_date' =>$date ,
                 'msg_time' =>$time
                 );
                 
            $this->db->insert("messages", $data);
        }
        header("Location: ".base_url("Dashboard/request"));
    }
    
    function requestresult()
    {
        $this->load->library('dashboard_menu');
        $this->load->view('template/header');
        $this->load->view('admin/');
        $this->load->view('template/footer');
    }
    
    function floodreport()
    {
        $this->load->library('dashboard_menu');
        $this->load->view('template/header');
		$this->load->view('admin/floodreport');
		$this->load->view('template/footer');
    }
    function barangay()
    {
        $this->load->library('dashboard_menu');
        $this->load->view('template/header');
		$this->load->view('admin/barangay');
		$this->load->view('template/footer');
    }
        function addbarangay()
        {
            $mun_id = $this->session->userdata('mun_id');
            $brgy_name = $_POST['brgy_name'];
            if($brgy_name == ""):
                echo 0;
            else:
                $data = array(
                             'brgy_id'=>NULL,
                             'mun_id' => $mun_id,
                             'brgy_name' => $brgy_name
                             );

                $this->db->insert('barangay',$data);

                echo 1;
            endif;
        }
        function displaybarangay()
        {
          $mun_id = $this->session->userdata('mun_id');
          $sql = $this->db->query("SELECT * FROM barangay where mun_id = ".$mun_id);
          foreach($sql->result() as $row):
              echo '
                  <ul class="row clearfix">
                      <li><h4>'.$row->brgy_name.'</h4></li>
                  </ul>
                   ';
          endforeach;
        }  
    function receivemessage()
    {
   
          require APPPATH . '../chikka-master/src/Chikka.php';

          $credentials = array(
            'client_id' => '33313f83a8b19ab72d260a35204e625e3d51b323edca8fa4c49f5a358acf361c',
            'secret_key'=> '1dd6823f661b8b494197feb92b2f167c35092d28efbc122212858d0eb80eb249',
            'shortcode' => '2929083668'
          );
          $mun_id = "";
          $dept_id = "";
          
          $title = "";
          $key = "";
          $name="";
          $department = "";
          $zip_code = "";
          
          $chikka = new Chikka($credentials);
          $chikka->receiveMessage(function($message) {
          $reply="";

          $string = $message->message;
          $j = explode(" ", $string);

          $date = date("mm/dd/yyyy");
          $time = date("h:i:s A");
          $key = $j[1];           
            
          if($key == 'REG'){
             $l = 1;
            for($i = 0 ; $i < sizeof($j) ; $i++):
                if($l == 1):
                    $title = $j[$i];
                    $l++;    
                elseif($l == 2):
                    $key = $j[$i];
                    $l++;
                elseif($l == 3):
                    $name = $j[$i];
                    $l++;
                elseif($l == 4):
                    $department = $j[$i];
                    $l++;
                elseif($l == 5):
                    $zip_code = $j[$i];
                endif;
            endfor;
            
            $sql = $this->db->query("select * from department where dept_name like '%".$department."'");
            foreach($sql->result() as $row):
                $dept_id = $row->dept_id;
            endforeach;
            if($zip_code != ""):
            
            $sql = $this->db->query("SELECT * from municipals where mun_zcode = '".$zip_code."'");
            foreach($sql->result() as $row):
                $mun_id = $row->mun_id;
            endforeach;
            else:
                $mun_id = 0;
            endif;
            $reply = $dept_id.', '.$mun_id;
//            $reply = "select * from department where dept_name like '%".$department."%'";
//            $reply = $title.', '.$key.', '.$name.', '.$department.', '.$zip_code;
            $data = array(
                         'ad_id' => '1',
                         'cn_id' => NULL,
                         'cn_name' => $name,
                         'cn_number' => $message->mobile_number,
                         'dept_id' =>$dept_id,
                         'mun_id' =>$mun_id,
                         'status' =>0
                         );            
            $this->db->insert('contacts',$data);
            $reply = "iSendisaster: Please wait for the approval message of the administrator. Thank You";
          }    
            
          $message->reply($reply, 0);
          return true;
      });
     
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */