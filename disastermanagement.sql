-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: 50.62.209.41:3306
-- Generation Time: Apr 25, 2015 at 06:05 AM
-- Server version: 5.5.40-36.1-log
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `disastermanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
`ad_id` int(11) NOT NULL,
  `cn_id` int(11) NOT NULL,
  `mun_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `ad_mail` varchar(60) NOT NULL,
  `ad_username` varchar(120) NOT NULL,
  `ad_password` varchar(120) NOT NULL,
  `ad_lastname` varchar(60) NOT NULL,
  `ad_middlename` varchar(60) NOT NULL,
  `ad_firstname` varchar(60) NOT NULL,
  `ad_contact_no` varchar(60) NOT NULL,
  `ad_address_no` varchar(60) NOT NULL,
  `ad_address_brgy` varchar(60) NOT NULL,
  `ad_province` varchar(60) NOT NULL,
  `ad_access` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`ad_id`, `cn_id`, `mun_id`, `dept_id`, `ad_mail`, `ad_username`, `ad_password`, `ad_lastname`, `ad_middlename`, `ad_firstname`, `ad_contact_no`, `ad_address_no`, `ad_address_brgy`, `ad_province`, `ad_access`) VALUES
(1, 0, 0, 0, 'jeimzm@gmail.com', 'aeda77527e83076ec42b0ec2d880d066', '8be94dd00b806f02ba760b745f7294eb ', 'Medina', 'Dela Cruz', 'Jeimson', '091032165', '85', 'Lagasit', 'Pangasinan', 1),
(55, 1, 18, 1, '', '06fc27aa3f20c5740e24a174452aff63', '3d4265da2fafe30fbf760b8a0998bb6e', '', '', 'Jeimson', '639104640155', '', '', '', 2),
(56, 2, 27, 1, '', '6fb7c68559f45f30e07ee2612b7c4aa5', '2cb45b4535018fc609fc0d9c42814c9a', '', '', 'ANJO', '639486515662', '', '', '', 2),
(57, 3, 25, 1, '', 'bc45efddc288388f8b6096d065ddc436', 'bf8e4d134146ae12584cf0eedd222a51', '', '', 'Rachelle', '639288415292', '', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `barangay`
--

CREATE TABLE IF NOT EXISTS `barangay` (
`brgy_id` bigint(20) NOT NULL,
  `mun_id` int(11) NOT NULL,
  `brgy_name` varchar(60) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=525 ;

--
-- Dumping data for table `barangay`
--

INSERT INTO `barangay` (`brgy_id`, `mun_id`, `brgy_name`) VALUES
(4, 18, 'Nalsian'),
(5, 18, 'Bued'),
(6, 18, 'Longos'),
(7, 3, 'Allabon'),
(8, 3, 'Aloleng'),
(9, 3, 'Bangan-Oda'),
(10, 3, 'Baruan'),
(11, 3, 'Boboy'),
(12, 3, 'Cayungnan'),
(13, 3, 'Dangley'),
(14, 3, 'Gayusan'),
(15, 3, 'Macaboboni'),
(16, 3, 'Magsaysay'),
(17, 3, 'Namatucan'),
(18, 3, 'Patar'),
(19, 3, 'Poblacion East'),
(20, 3, 'Poblacion West'),
(21, 3, 'San Juan'),
(22, 3, 'Manlocboc'),
(23, 3, 'Tupa'),
(24, 3, 'Viga'),
(25, 4, 'Bayaoas'),
(26, 4, 'Bayaoas'),
(27, 4, 'Baybay'),
(28, 4, 'Bocacliw'),
(29, 4, 'Bocboc East'),
(30, 4, 'Balangobong'),
(31, 4, 'Balangobong'),
(32, 4, 'Bocboc West'),
(33, 4, 'Buer'),
(34, 4, 'Buer'),
(35, 4, 'Calsib'),
(36, 4, 'Niñoy'),
(37, 4, 'Poblacion'),
(38, 4, 'Pogomboa'),
(39, 4, 'Pogonsili'),
(40, 4, 'San Jose'),
(41, 4, 'Tampac'),
(42, 4, 'Laoag'),
(43, 4, 'Manlocboc'),
(44, 4, 'Panacol'),
(45, 5, 'Anulid'),
(46, 5, 'Atainan'),
(47, 5, 'Bersamin'),
(48, 5, 'Canarvacanan'),
(49, 5, 'Caranglaan'),
(50, 5, 'Curareng'),
(51, 5, 'Gualsic'),
(52, 5, 'Kisikis'),
(53, 5, 'Laoac'),
(54, 5, 'Macayo'),
(55, 5, 'Pindangan Centro'),
(56, 5, 'Pindangan East'),
(57, 5, 'Pindangan West'),
(58, 5, 'Poblacion East'),
(59, 5, 'Poblacion West'),
(60, 5, 'San Juan'),
(61, 5, 'San Nicolas'),
(62, 5, 'San Pedro Apartado'),
(63, 5, 'San Pedro Ili'),
(64, 5, 'San Vicente'),
(65, 5, 'Vacante'),
(66, 6, 'Awile'),
(67, 6, 'Awag'),
(68, 6, 'Batiarao'),
(69, 6, 'Cabungan'),
(70, 6, 'Carot'),
(71, 6, 'Dolaoan'),
(72, 6, 'Imbo'),
(73, 6, 'Macaleeng'),
(74, 6, 'Macando-candong'),
(75, 6, 'Mal-ong'),
(76, 6, 'Namagbagan'),
(77, 6, 'Poblacion'),
(78, 6, 'Roxas'),
(79, 6, 'Sablig'),
(80, 6, 'San Jose'),
(81, 6, 'Siapar'),
(82, 6, 'Tondol'),
(83, 6, 'Tori-tori'),
(84, 7, 'Ariston Este'),
(85, 7, 'Ariston Weste'),
(86, 7, 'Bantog'),
(87, 7, 'Baro'),
(88, 7, 'Cabalitian'),
(89, 7, 'Bobonan'),
(90, 7, 'Calepaan'),
(91, 7, 'Carosucan Norte'),
(92, 7, 'Carosucan Sur'),
(93, 7, 'Coldit'),
(94, 7, 'Domanpot'),
(95, 7, 'Dupac'),
(96, 7, 'Macalong'),
(97, 7, 'Palaris'),
(98, 7, 'Poblacion East'),
(99, 7, 'Poblacion West'),
(100, 7, 'San Vicente Este'),
(101, 7, 'San Vicente Weste'),
(102, 7, 'Sanchez'),
(103, 7, 'Sobol'),
(104, 7, 'Toboy'),
(105, 8, 'Angayan Norte'),
(106, 8, 'Angayan Sur'),
(107, 8, 'Capulaan'),
(108, 8, 'Esmeralda'),
(109, 8, 'Kita-kita'),
(110, 8, 'Mabini'),
(111, 8, ' Mauban'),
(112, 8, 'Poblacion'),
(113, 8, 'Pugaro'),
(114, 8, 'Rajal'),
(115, 8, 'San Andres'),
(116, 8, 'San Aurelio 1st'),
(117, 8, 'San Aurelio 2nd'),
(118, 8, 'San Aurelio 3rd'),
(119, 8, 'San Joaquin'),
(120, 8, 'San Julian'),
(121, 8, 'San Leon'),
(122, 8, 'San Marcelino'),
(123, 8, 'San Miguel'),
(124, 8, 'San Raymundo'),
(125, 9, 'Ambabaay'),
(126, 9, 'Aporao'),
(127, 9, 'Arwas'),
(128, 9, 'Ballag'),
(129, 9, 'Banog Norte'),
(130, 9, 'Banog Sur'),
(131, 9, 'Calabeng'),
(132, 9, 'Centro Toma'),
(133, 9, 'Colayo'),
(134, 9, 'Dacap Norte'),
(135, 9, 'Dacap Sur'),
(136, 9, 'Garrita'),
(137, 9, 'Luac'),
(138, 9, 'Macabit'),
(139, 9, 'Masidem'),
(140, 9, 'Masidem'),
(141, 9, 'Poblacion'),
(142, 9, 'Quinaoayanan'),
(143, 9, 'Ranao'),
(144, 9, 'Ranom Iloco'),
(145, 9, 'San Jose'),
(146, 9, ' San Miguel'),
(147, 9, 'San Simon'),
(148, 9, 'San Vicente'),
(149, 9, 'Tiep'),
(150, 9, 'Tagumbao'),
(151, 9, 'Tipor'),
(152, 9, 'Tugui Grande'),
(153, 9, 'Tugui Norte'),
(154, 10, 'Anambongan'),
(155, 10, 'Bayoyong'),
(156, 10, 'Cabeldatan'),
(157, 10, 'Dumpay'),
(158, 10, 'Malimpec East'),
(159, 10, 'Mapolopolo'),
(160, 10, 'Nalneran'),
(161, 10, 'Navatat'),
(162, 10, 'Obong'),
(163, 10, 'Osmena, Sr.'),
(164, 10, 'Palma'),
(165, 10, 'Patacbo'),
(166, 10, 'Poblacion'),
(167, 11, 'Artacho'),
(168, 11, 'Baluyot'),
(169, 11, 'Bersamin'),
(170, 11, 'Cabuaan'),
(171, 11, 'Cacandongan'),
(172, 11, 'Diaz'),
(173, 11, 'Nandacan'),
(174, 11, 'Nibaliw Norte'),
(175, 11, 'Nibaliw Sur'),
(176, 11, 'Palisoc'),
(177, 11, 'Poblacion East'),
(178, 11, 'Poblacion West'),
(179, 11, 'Pogo'),
(180, 11, 'Poponto'),
(181, 11, 'Primicias'),
(182, 11, 'Ketegan'),
(183, 11, 'Sinabaan'),
(184, 11, 'Vacante'),
(185, 11, 'Villanueva'),
(186, 12, 'Alinggan'),
(187, 12, 'Amamperez'),
(188, 12, 'Amancosiling Norte'),
(189, 12, 'Amancosiling Sur'),
(190, 12, 'Ambayat I'),
(191, 12, 'Ambayat II'),
(192, 12, 'Apalen'),
(193, 12, 'Asin'),
(194, 12, 'Ataynan'),
(195, 12, 'Bacnono'),
(196, 12, 'Balaybuaya'),
(197, 12, 'Banaban'),
(198, 12, 'Bani'),
(199, 12, 'Batangcawa'),
(200, 12, 'Beleng'),
(201, 12, 'Bical Norte'),
(202, 12, 'Bical Sur'),
(203, 12, 'Bongato East'),
(204, 12, 'Bongato West'),
(205, 12, 'Buayaen'),
(206, 12, 'Buenlag 1st'),
(207, 12, 'Buenlag 2nd'),
(208, 12, 'Cadre Site'),
(209, 12, 'Carungay'),
(210, 12, 'Caturay'),
(211, 12, 'Darawey (Tangal)'),
(212, 12, 'Duera'),
(213, 12, 'Dusoc'),
(214, 12, 'Hermoza'),
(215, 12, 'Inanlorenza'),
(216, 12, 'Inirangan'),
(217, 12, 'Iton'),
(218, 12, 'Langiran'),
(219, 12, 'Ligue'),
(220, 12, 'M. H. del Pilar'),
(221, 12, 'Macayocayo'),
(222, 12, 'Magsaysay'),
(223, 12, 'Maigpa'),
(224, 12, 'Malimpec'),
(225, 12, 'Malioer'),
(226, 12, 'Managos'),
(227, 12, 'Manambong Norte'),
(228, 12, ' Manambong Parte'),
(229, 12, 'Manambong Sur'),
(230, 12, 'Mangayao'),
(231, 12, 'Nalsian Norte'),
(232, 12, 'Nalsian Sur'),
(233, 12, 'Pangdel'),
(234, 12, 'Pantol'),
(235, 12, 'Pantol'),
(236, 12, 'Paragos'),
(237, 12, 'Poblacion Sur'),
(238, 12, 'Pugo'),
(239, 12, 'Reynado'),
(240, 12, 'San Gabriel 1st'),
(241, 12, 'San Gabriel 2nd'),
(242, 12, 'San Vicente'),
(243, 12, 'Sangcagulis'),
(244, 12, 'Sanlibo'),
(245, 12, 'Sapang'),
(246, 12, 'Tamaro'),
(247, 12, 'Tambac'),
(248, 12, 'Tampog'),
(249, 12, 'Tanolong'),
(250, 12, 'Tatarac'),
(251, 12, 'Telbang'),
(252, 12, 'Tococ East'),
(253, 12, 'Tococ West'),
(254, 12, 'Warding'),
(255, 12, 'Wawa'),
(256, 12, 'Zone I (Pob.)'),
(257, 12, 'Zone II (Pob.)'),
(258, 12, 'Zone III (Pob.)'),
(259, 12, 'Zone IV (Pob.)'),
(260, 12, 'Zone V (Pob.)'),
(261, 12, 'Zone VI (Pob.)'),
(262, 12, 'Zone VII (Pob.)'),
(263, 13, 'Balangobong'),
(264, 13, 'Balangobong'),
(265, 13, 'Bued'),
(266, 13, 'Bugayong'),
(267, 13, 'Camangaan'),
(268, 13, 'Canarvacanan'),
(269, 13, 'Capas'),
(270, 13, 'Cili'),
(271, 13, 'Dumayat'),
(272, 13, 'Linmansangan'),
(273, 13, 'Mangcasuy'),
(274, 13, 'Moreno'),
(275, 13, 'Pasileng Norte'),
(276, 13, 'Pasileng Sur'),
(277, 13, 'Poblacion'),
(278, 13, 'San Felipe Central'),
(279, 13, 'San Felipe Sur'),
(280, 13, 'San Pablo'),
(281, 13, 'Santa Catalina'),
(282, 13, 'Santa Maria Norte'),
(283, 13, 'Santiago'),
(284, 13, 'Santo Niño'),
(285, 13, 'Tabuyoc'),
(286, 13, 'Vacante'),
(287, 14, 'Amancoro'),
(288, 14, 'Balagan'),
(289, 14, 'Balogo'),
(290, 14, 'Basing'),
(291, 14, 'Baybay Lopez'),
(292, 14, 'Baybay Polong'),
(293, 14, 'Biec'),
(294, 14, 'Buenlag'),
(295, 14, 'Calit'),
(296, 14, 'Caloocan Dupo'),
(297, 14, 'Caloocan Sur'),
(298, 14, 'Camaley'),
(299, 14, 'Canaoalan'),
(300, 14, 'Dulag'),
(301, 14, 'Gayaman'),
(302, 14, 'Linoc'),
(303, 14, 'Lomboy'),
(304, 14, 'Nagpalangan'),
(305, 14, 'Malindong'),
(306, 14, 'Manat'),
(307, 14, 'Naguilayan'),
(308, 14, 'Pallas'),
(309, 14, 'Papagueyan'),
(310, 14, 'Parayao'),
(311, 14, 'Poblacion'),
(312, 14, 'Pototan'),
(313, 14, 'Sabangan'),
(314, 14, 'Salapingao'),
(315, 14, 'San Isidro Norte'),
(316, 14, 'San Isidro Sur'),
(317, 14, 'Santa Rosa'),
(318, 14, 'Tombor'),
(319, 15, 'Arnedo'),
(320, 15, 'Balingasay'),
(321, 15, 'Binabalian'),
(322, 15, 'Cabuyao'),
(323, 15, 'Catuday'),
(324, 15, 'Catungi'),
(325, 15, 'Concordia (Pob.)'),
(326, 15, 'Culang'),
(327, 15, 'Dewey'),
(328, 15, 'Estanza'),
(329, 15, 'Germinal (Pob.)'),
(330, 15, 'Goyoden'),
(331, 15, 'Ilog-Malino'),
(332, 15, 'Lambes'),
(333, 15, 'Liwa-liwa'),
(334, 15, 'Lucero'),
(335, 15, 'Luciente 1.0 (J.Celeste)'),
(336, 15, 'Luciente 2.0'),
(337, 15, 'Patar'),
(338, 15, 'Pilar'),
(339, 15, 'Salud'),
(340, 15, 'Samang Norte'),
(341, 15, 'Samang Sur'),
(342, 15, 'Sampaloc'),
(343, 15, 'San Roque'),
(344, 15, '   Tara'),
(345, 15, 'Tagapuro'),
(346, 15, 'Tupa'),
(347, 15, 'Victory'),
(348, 15, 'Zaragoza'),
(349, 16, 'Angarian'),
(350, 16, 'Asinan'),
(351, 16, 'Bañaga'),
(352, 16, 'Bacabac'),
(353, 16, 'Bolaoen'),
(354, 16, 'Buenlag'),
(355, 16, 'Cabayaoasan'),
(356, 16, 'Cayanga'),
(357, 16, 'Gueset'),
(358, 16, 'Hacienda'),
(359, 16, 'Laguit Centro'),
(360, 16, 'Laguit Padilla'),
(361, 16, 'Magtaking'),
(362, 16, 'Pangascasan'),
(363, 16, 'Pantal'),
(364, 16, 'Poblacion'),
(365, 16, 'Polong'),
(366, 16, 'Portic'),
(367, 16, 'Salasa'),
(368, 16, 'Salomague Norte'),
(369, 16, 'Salomague Sur'),
(370, 16, 'Samat'),
(371, 16, 'San Francisco'),
(372, 16, 'Umanday'),
(373, 17, 'Anapao'),
(374, 17, 'Cacayasen'),
(375, 17, 'Concordia'),
(376, 17, 'Ilio-ilio (Iliw-iliw)'),
(377, 17, 'Papallasen'),
(378, 17, 'Poblacion'),
(379, 17, 'Pogoruac'),
(380, 17, 'Don Matias'),
(381, 17, 'San Miguel'),
(382, 17, 'San Pascual'),
(383, 17, 'San Vicente'),
(384, 17, 'Sapa Grande'),
(385, 17, 'Sapa Pequeña'),
(386, 17, 'Tambacan'),
(387, 18, 'Ambonao'),
(388, 18, 'Ambuetel'),
(389, 18, 'Banaoang'),
(390, 18, 'Buenlag'),
(391, 18, 'Cabilocaan'),
(392, 18, 'Dinalaoan'),
(393, 18, 'Doyong'),
(394, 18, 'Gabon'),
(395, 18, 'Lasip'),
(396, 18, 'Lumbang'),
(397, 18, 'Macabito'),
(398, 18, 'Malabago'),
(399, 18, 'Mancup'),
(400, 18, 'Nagsaing'),
(401, 18, 'Poblacion East'),
(402, 18, 'Poblacion West'),
(403, 18, 'Quesban'),
(404, 18, 'San Miguel'),
(405, 18, 'San Vicente'),
(406, 18, 'Songkoy'),
(407, 18, 'Talibaew'),
(408, 0, 'Alilao'),
(409, 0, 'Amalbalan'),
(410, 19, 'Alilao'),
(411, 19, 'Amalbalan'),
(412, 19, 'Bobonot'),
(413, 19, 'Eguia'),
(414, 19, 'Gais-Guipe'),
(415, 19, 'Hermosa'),
(416, 19, 'Macalang'),
(417, 19, 'Magsaysay'),
(418, 19, 'Malacapas'),
(419, 19, 'Malimpin'),
(420, 19, 'Osmeña'),
(421, 19, 'Petal'),
(422, 19, 'Poblacion'),
(423, 19, 'San Vicente'),
(424, 19, 'Tambac'),
(425, 19, 'Tambobong'),
(426, 19, 'Uli'),
(427, 19, 'Viga'),
(428, 20, 'Bamban'),
(429, 20, 'Batang'),
(430, 20, 'Bayambang'),
(431, 20, 'Cato'),
(432, 20, 'Doliman'),
(433, 20, 'Patima'),
(434, 20, 'Maya'),
(435, 20, 'Nangalisan'),
(436, 20, 'Nayom'),
(437, 20, 'Pita'),
(438, 20, 'Poblacion'),
(439, 20, 'Potol'),
(440, 20, 'Babuyan'),
(441, 21, 'Bolo'),
(442, 0, 'Bolo'),
(443, 0, 'Bongalon'),
(444, 21, 'Bongalon'),
(445, 21, 'Dulig'),
(446, 21, 'Laois'),
(447, 21, 'Magsaysay'),
(448, 21, 'Poblacion'),
(449, 21, 'San Gonzalo'),
(450, 21, 'San Jose'),
(451, 21, 'Tobuan'),
(452, 21, 'Uyong'),
(453, 22, 'Anis'),
(454, 22, 'Balligi'),
(455, 22, 'Banuar'),
(456, 22, 'Botigue'),
(457, 22, 'Caaringayan'),
(458, 22, 'Domingo Alarcio'),
(459, 22, 'Cabilaoan'),
(460, 22, 'Cabulalaan'),
(461, 22, 'Cabulalaan'),
(462, 22, 'Calaoagan'),
(463, 22, 'Calmay'),
(464, 22, 'Casampagaan'),
(465, 22, 'Casanestebanan'),
(466, 22, 'Casantiagoan'),
(467, 22, 'Inmanduyan'),
(468, 22, 'Poblacion'),
(469, 22, 'Lebueg'),
(470, 22, 'Maraboc'),
(471, 22, 'Nanbagatan'),
(472, 22, 'Panaga'),
(473, 22, 'Talogtog'),
(474, 22, 'Turko'),
(475, 22, 'Yatyat'),
(476, 23, 'Aliwekwek'),
(477, 23, 'Baay'),
(478, 23, 'Balangobong'),
(479, 23, 'Balococ'),
(480, 23, 'Bantayan'),
(481, 23, 'Basing'),
(482, 23, 'Capandanan'),
(483, 23, 'Domalandan Center'),
(484, 23, 'Domalandan East'),
(485, 23, 'Domalandan West'),
(486, 23, 'Domalandan East'),
(487, 23, 'Dorongan'),
(488, 23, 'Dulag'),
(489, 23, 'Estanza'),
(490, 23, 'Lasip'),
(491, 23, 'Libsong East'),
(492, 23, 'Libsong West'),
(493, 23, 'Malawa'),
(494, 23, 'Malimpuec'),
(495, 23, 'Maniboc'),
(496, 23, 'Matalava'),
(497, 23, 'Naguelguel'),
(498, 23, 'Namolan'),
(499, 23, 'Pangapisan North'),
(500, 23, 'Pangapisan Sur'),
(501, 23, 'Poblacion'),
(502, 23, 'Quibaol'),
(503, 23, 'Rosario'),
(504, 23, 'Sabangan'),
(505, 23, 'Talogtog'),
(506, 23, 'Tonton'),
(507, 23, 'Tumbar'),
(508, 23, 'Wawa'),
(509, 24, 'Bacnit'),
(510, 24, 'Barlo'),
(511, 24, 'Caabiangaan'),
(512, 24, 'Cabanaetan'),
(513, 24, 'Cabinuangan'),
(514, 24, 'Calzada'),
(515, 24, 'Caranglaan'),
(516, 24, 'De Guzman'),
(517, 24, 'Luna '),
(518, 24, 'Magalong'),
(519, 24, 'Nibaliw'),
(520, 24, 'Patar'),
(521, 24, 'Poblacion'),
(522, 24, 'San Pedro'),
(523, 24, 'Tagudin'),
(524, 24, 'Villacorta');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`cat_id` bigint(20) NOT NULL,
  `cat_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('6db1ff6b443e75a060e48eda22b71539', '115.147.109.32', 'Mozilla/5.0 (Linux; Android 4.4.2; SM-T311 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.108 Saf', 1429894789, ''),
('007afa8eaa4a35d020d500bd7df08294', '122.52.230.46', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429597023, ''),
('70beeb3fc1123a453ef29faa93d030e9', '122.52.230.46', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', 1429595708, 'a:7:{s:9:"user_data";s:0:"";s:5:"ad_id";s:1:"1";s:6:"mun_id";s:1:"0";s:10:"ad_dept_id";s:1:"0";s:5:"cn_id";s:1:"0";s:9:"ad_access";s:1:"1";s:9:"logged_in";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `ad_id` int(11) NOT NULL DEFAULT '1',
`cn_id` int(11) NOT NULL,
  `cn_name` varchar(50) NOT NULL,
  `cn_number` varchar(60) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `mun_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`ad_id`, `cn_id`, `cn_name`, `cn_number`, `dept_id`, `mun_id`, `status`) VALUES
(1, 1, 'Jeimson', '639104640155', 1, 37, 1),
(1, 2, 'ANJO', '639486515662', 1, 27, 1),
(1, 3, 'Rachelle', '639288415292', 1, 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_group`
--

CREATE TABLE IF NOT EXISTS `contact_group` (
`grp_id` int(11) NOT NULL,
  `grp_name` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_type`
--

CREATE TABLE IF NOT EXISTS `contact_type` (
`ctype_id` int(11) NOT NULL,
  `ctype_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
`dept_id` int(11) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `mun_id` int(11) NOT NULL,
  `dept_name` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_id`, `ad_id`, `mun_id`, `dept_name`) VALUES
(1, 1, 0, 'MDRRMC'),
(2, 1, 0, 'PPNP'),
(3, 1, 0, 'DSWD'),
(4, 1, 0, 'PHO'),
(5, 1, 0, 'PAGO'),
(6, 1, 0, 'PIO'),
(7, 1, 0, 'Regular Users');

-- --------------------------------------------------------

--
-- Table structure for table `flood_level`
--

CREATE TABLE IF NOT EXISTS `flood_level` (
`fl_id` int(11) NOT NULL,
  `fl_lvlname` varchar(25) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `flood_level`
--

INSERT INTO `flood_level` (`fl_id`, `fl_lvlname`) VALUES
(1, 'LOW'),
(2, 'MID'),
(3, 'HIGH');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `ad_id` int(11) NOT NULL,
  `req_id` int(11) NOT NULL,
`msg_id` int(11) NOT NULL,
  `msg_to` varchar(60) NOT NULL,
  `msg_details` text NOT NULL,
  `msg_date` varchar(60) NOT NULL,
  `msg_time` varchar(60) NOT NULL,
  `msg_mark` int(11) NOT NULL,
  `msg_remarks` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `municipals`
--

CREATE TABLE IF NOT EXISTS `municipals` (
`mun_id` int(11) NOT NULL,
  `mun_zcode` varchar(25) NOT NULL,
  `mun_name` varchar(80) NOT NULL,
  `mun_image` varchar(80) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `municipals`
--

INSERT INTO `municipals` (`mun_id`, `mun_zcode`, `mun_name`, `mun_image`) VALUES
(3, '2408', 'Agno', 'images/mun_images/Agno_Pangasinan.png'),
(4, '2415', 'Aguilar', 'images/mun_images/Aguilar_Pangasinan.png'),
(5, '2425', 'Alcala', 'images/mun_images/Alcala_Pangasinan.png'),
(6, '2405', 'Anda', 'images/mun_images/Anda_Pangasinan.png'),
(7, '2439', 'Asingan', 'images/mun_images/Asingan_Pangasinan.png'),
(8, '2442', 'Balungao', 'images/mun_images/Balungao.png'),
(9, '2407', 'Bani', 'images/mun_images/Bani_Pangasinan.png'),
(10, '2422', 'Basista', 'images/mun_images/Basista_Pangasinan.png'),
(11, '2424', 'Bautista', 'images/mun_images/Ph_seal_bautista_pangasinan.png'),
(12, '2423', 'Bayambang', 'images/mun_images/Bayambang_Pangasinan.png'),
(13, '2436', 'Binalonan', 'images/mun_images/Binalonan_Pangasinan.png'),
(14, '2417', 'Binmaley', 'images/mun_images/Binmaley_Pangasinan.png'),
(15, '2406', 'Bolinao', 'images/mun_images/Bolinao_Pangasinan.png'),
(16, '2416', 'Bugallon', 'images/mun_images/Bugallon_Pangasinan.png'),
(17, '2410', 'Burgos', 'images/mun_images/Burgos_Pangasinan.png'),
(18, '2418', 'Calasiao', 'images/mun_images/Calasiao_Pangasinan.png'),
(19, '2411', 'Dasol', 'images/mun_images/Dasol_Pangasinan.png'),
(20, '2412', 'Infanta', 'images/mun_images/Infanta_Pangasinan.png'),
(21, '2402', 'Labrador', 'images/mun_images/Labrador_Pangasinan.png'),
(22, '2437', 'Laoac', 'images/mun_images/Laoac_Pangasinan.png'),
(23, '2401', 'Lingayen', 'images/mun_images/Lingayen_Pangasinan.png'),
(24, '2409', 'Mabini', 'images/mun_images/Mabini_Pangasinan.png'),
(25, '2421', 'Malasiqui', 'images/mun_images/Malasiqui_Pangasinan.png'),
(26, '2430', 'Manaoag', 'images/mun_images/Manaoag_Pangasinan.png'),
(27, '2432', 'Mangaldan', 'images/mun_images/Mangaldan_Pangasinan.png'),
(28, '2413', 'Mangatarem', 'images/mun_images/Mangatarem_Pangasinan.png'),
(29, '2429', 'Mapandan', 'images/mun_images/Mapandan_Pangasinan.png'),
(30, '2446', 'Natividad', 'images/mun_images/Natividad_Pangasinan.png'),
(31, '2435', 'Pozzorubio', 'images/mun_images/Pozorrubio_Pangasinan.png'),
(32, '2441', 'Rosales', 'images/mun_images/Rosales_Pangasinan.png'),
(33, '', 'San Fabian', 'images/mun_images/San_Fabian_Pangasinan.png'),
(34, '2431', 'San Jacinto', 'images/mun_images/San_Jacinto_Pangasinan.png'),
(35, '', 'San Manuel', 'images/mun_images/San_Manuel_Pangasinan.png'),
(36, '2447', 'San Nicolas', 'images/mun_images/San_Nicolas_Pangasinan.png'),
(37, '2444', 'San Quintin', 'images/mun_images/San_Quintin_Pangasinan.png'),
(38, '2419', 'Santa Barbara', 'images/mun_images/Santa-barbara.png'),
(39, '2440', 'Santa Maria', 'images/mun_images/Santa_Maria_Pangasinan.png'),
(40, '2426', 'Santo Tomas', 'images/mun_images/Santo_Tomas_Pangasinan.png'),
(41, '2434', 'Sison', 'images/mun_images/Sison_Pangasinan.png'),
(42, '2403', 'Sual', 'images/mun_images/Sual_Pangasinan.png'),
(43, '2445', 'Tayug', 'images/mun_images/Tayug_Pangasinan.png'),
(44, '2443', 'Umingan', 'images/mun_images/Umingan_Pangasinan.png'),
(45, '2414', 'Urbiztondo', 'images/mun_images/Urbiztondo_Pangasinan.png'),
(46, '2427', 'Villasis', 'images/mun_images/Villasis_Pangasinan.png');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `ad_id` int(11) NOT NULL,
`post_id` int(11) NOT NULL,
  `post_title` varchar(120) NOT NULL,
  `post_description` text NOT NULL,
  `post_date` varchar(60) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `post_fet_img` varchar(120) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`ad_id`, `post_id`, `post_title`, `post_description`, `post_date`, `cat_id`, `post_fet_img`) VALUES
(55, 1, 'This is A Sample Post', 'This is a sample Message', '0404/0909/15151515 04:57:36 PM', 0, ''),
(57, 2, 'Attention!', 'This is only a testing. Please bear with us. Thankyou :)', '0404/1010/15151515 05:39:01 AM', 0, ''),
(57, 3, 'Testing', 'This is just a testing. Please bear with us. Thankyou :)', '0404/1010/15151515 05:42:09 AM', 0, ''),
(56, 4, 'Testing ', 'This is just a test. ', '0404/1010/15151515 05:45:12 AM', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE IF NOT EXISTS `report` (
`rep_id` int(11) NOT NULL,
  `cn_no` varchar(50) NOT NULL,
  `rep_date` varchar(50) NOT NULL,
  `rep_time` varchar(60) NOT NULL,
  `brgy_id` varchar(50) NOT NULL,
  `rep_flevel` varchar(50) NOT NULL,
  `rep_desc` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `ad_id` int(11) NOT NULL,
  `brgy_id` int(11) NOT NULL,
`req_id` int(11) NOT NULL,
  `req_type` varchar(50) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `req_details` text NOT NULL,
  `req_time` varchar(50) NOT NULL,
  `req_date` varchar(50) NOT NULL,
  `req_stat` varchar(50) NOT NULL,
  `req_mark` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`ad_id`, `brgy_id`, `req_id`, `req_type`, `mobile_number`, `req_details`, `req_time`, `req_date`, `req_stat`, `req_mark`) VALUES
(55, 4, 2, '3', '639158186928', 'We nee Help', '06:32 AM', '01/29/2015', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `request_type`
--

CREATE TABLE IF NOT EXISTS `request_type` (
  `rtype_id` int(11) NOT NULL,
  `rtype_name` varchar(50) NOT NULL,
  `req_acyr` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `request_type`
--

INSERT INTO `request_type` (`rtype_id`, `rtype_name`, `req_acyr`) VALUES
(1, 'Food', 'FOOD'),
(2, 'Medicine', 'MED'),
(3, 'Rescue', 'RES'),
(4, 'Evacuation', 'EVAC'),
(5, 'Construction', 'CONS');

-- --------------------------------------------------------

--
-- Table structure for table `subscriber`
--

CREATE TABLE IF NOT EXISTS `subscriber` (
  `mun_id` int(11) NOT NULL,
`sub_id` int(11) NOT NULL,
  `sub_number` varchar(60) NOT NULL,
  `sub_active` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `subscriber`
--

INSERT INTO `subscriber` (`mun_id`, `sub_id`, `sub_number`, `sub_active`) VALUES
(46, 14, '639158186928', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
 ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `barangay`
--
ALTER TABLE `barangay`
 ADD PRIMARY KEY (`brgy_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
 ADD PRIMARY KEY (`cn_id`);

--
-- Indexes for table `contact_group`
--
ALTER TABLE `contact_group`
 ADD PRIMARY KEY (`grp_id`);

--
-- Indexes for table `contact_type`
--
ALTER TABLE `contact_type`
 ADD PRIMARY KEY (`ctype_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `flood_level`
--
ALTER TABLE `flood_level`
 ADD PRIMARY KEY (`fl_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `municipals`
--
ALTER TABLE `municipals`
 ADD PRIMARY KEY (`mun_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
 ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
 ADD PRIMARY KEY (`rep_id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
 ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `request_type`
--
ALTER TABLE `request_type`
 ADD PRIMARY KEY (`rtype_id`);

--
-- Indexes for table `subscriber`
--
ALTER TABLE `subscriber`
 ADD PRIMARY KEY (`sub_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `barangay`
--
ALTER TABLE `barangay`
MODIFY `brgy_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=525;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `cat_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
MODIFY `cn_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contact_group`
--
ALTER TABLE `contact_group`
MODIFY `grp_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_type`
--
ALTER TABLE `contact_type`
MODIFY `ctype_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `flood_level`
--
ALTER TABLE `flood_level`
MODIFY `fl_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `municipals`
--
ALTER TABLE `municipals`
MODIFY `mun_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
MODIFY `rep_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
MODIFY `req_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subscriber`
--
ALTER TABLE `subscriber`
MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
