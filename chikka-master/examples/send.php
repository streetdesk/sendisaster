<?php

  /**
   * Sending SMS with the Chikka API
   */

  // Require Chikka SDK file
  require('../src/Chikka.php');

  // Set credentials
  $credentials = array(
    'client_id' => '33313f83a8b19ab72d260a35204e625e3d51b323edca8fa4c49f5a358acf361c',
    'secret_key'=> '1dd6823f661b8b494197feb92b2f167c35092d28efbc122212858d0eb80eb249',
    'shortcode' => '2929083668'
  );

  // Instantiate Chikka (passing credentials)
  $chikka = new Chikka($credentials);

  // Mobile number and message
  // Mobile number can have the prefix +63, 63, or 0
  $mobileNumber = '09104640155';
  $message = 'Hello world';

  // Send SMS ($send will contain the Chikka_Response object)
  $send = $chikka->send($mobileNumber, $message);

  // If you don't want to specify a `message_id` as a 3rd parameter in the send() function,
  // A `message_id` is automatically generated (16 digits)
  // You can retrieve the `message_id` through the following
  $messageId = $send->msg->message_id;

  // Check if message was sent
  if ($send->success()) {

    echo 'Message successfully sent';
  } else {
    // Print error message
    echo 'Message not sent. ', $send->message;
  }