/*==============================HOME PAGE==================================*/
/*
LOGIN FORM    Validations
*/
$(function(){
    $('.close-notify').click(function(){
            $('#notify').fadeOut();
    });
})
$(function(){
   $('#user').focus(function(){
        this.select();
    });
})
$(function(){
    $('#pass').click(function(){
           $('#pass').val("");
    });
})

$(function(){
    $('#login-button').click(function(){
        var user = $('#user').val();
        var pass = $('#pass').val();
        $('#login-button').hide();
        $('.login-loader').show();
        if(user == "" && pass == ""){
            $('#notify').removeClass("notify-ok");
            $('#notify').addClass("notify-err");
            $('#notify').fadeIn();
            $('.notify-desc').html("Please enter your username and password");
            var t3=setTimeout(function(){$('#notify').fadeOut();},20000);
        }else{
            $.post('home/loguser',{username:user, password:pass },function(result){
                  //alert(result);
                $('#login-button').show();
                 $('.login-loader').hide(); 
                if(result != 0){
                    window.location = "Dashboard/";
                } else {
                    $('#notify').removeClass("notify-ok");
                    $('#notify').addClass("notify-err");
                    $('#notify').fadeIn();
                    $('.notify-desc').html("Incorrect Username/Password");
                    $('#notify').fadeOut();
                    $('#login-button').show();
                } 
            });
        
        }
         
    });
})

/*==============================DASHBOARD PAGE==================================*/

$(function(){
    $('#ch_0').click(function(){
         if($('#ch_0').is(':checked')){
            $('.check_box').prop('checked', true); 
             $('#temp_dept_id').val("all");
         }else{
            $('.check_box').prop('checked', false);
            $('#temp_dept_id').val("");
         }
        
    });
})
/*
    DASHBOARD: disseminate
*/
$(function(){
    $('#save-dept').click(function(){
        var dept = $('#dept').val();
        
        $.post("../Dashboard/adddepartment",{dept:dept},function(result){
            console.log(result);
            if(result == 1){
                $('#notify').removeClass("notify-ok");
                $('#notify').removeClass("notify-err");
                $('#notify').addClass("notify-ok");
                $('#notify').fadeIn();
                $('.notify-desc').html("Saved!");
                window.location = "../Dashboard/";
            }else{
                $('#notify').removeClass("notify-err");
                $('#notify').removeClass("notify-ok");
                $('#notify').addClass("notify-err");
                $('#notify').fadeIn();
                $('.notify-desc').html("Seems like the input is empty.");
            }
        });
    })
})

/*
    DASHBOARD: barangay
*/

$(function(){
    $('#save-brgy').click(function(){
        var brgy_name = $('#brgy_name').val();
        
        $.post("../Dashboard/addbarangay",{brgy_name:brgy_name},function(result){
            console.log(result);
            if(result == 1){
                $('#notify').removeClass("notify-ok");
                $('#notify').removeClass("notify-err");
                $('#notify').addClass("notify-ok");
                $('#notify').fadeIn();
                $('.notify-desc').html("Saved!");
                 $.post("../Dashboard/displaybarangay",{},function(result){
                     $('#barangay-list').html(result);
                 })
            }else{
                $('#notify').removeClass("notify-err");
                $('#notify').removeClass("notify-ok");
                $('#notify').addClass("notify-err");
                $('#notify').fadeIn();
                $('.notify-desc').html("Seems like the input is empty.");
            }
        });
    });
})
/*
    DASHBOARD: issue request
*/
//
//$(function(){
//    $('#send-request').click(function(){
//        var temp_dept_id = $('#temp_dept_id').val();
//        var message = $('#message').val();
//        var req_id = $('#req_id').val();
//        $.post("../Dashboard/sendissuerequest",{temp_dept_id:temp_dept_id,message:message},function(result){
//        
//        });
//    });
//})
/*
    DASHBOARD: request
*/

$(function(){
    $('#approvaling').submit(function(){
        var conf = confirm("Are You sure?");
        if(conf == true){
                return true;
        }else{
            return false;
        }
    });
})

/*==============================PREPAREDNESS PAGE==================================*/
$(function(){
    $('#sub-menu-one').click(function(){
        var frame = 1;
        $.post("preparedness/loadvideo",{frame:frame},function(result){
                $('#fvid').attr('src', result);
        });
        $.post("preparedness/loadpdf",{frame:frame},function(result){
                $('#pdf').attr('src', result);
        });
    });
})
$(function(){
    $('#sub-menu-two').click(function(){
        var frame = 2;
        $.post("preparedness/loadvideo",{frame:frame},function(result){
                $('#fvid').attr('src', result);
        });
        $.post("preparedness/loadpdf",{frame:frame},function(result){
                $('#pdf').attr('src', result);
        });
    });
})
$(function(){
    $('#sub-menu-three').click(function(){
        var frame = 3;
        $.post("preparedness/loadvideo",{frame:frame},function(result){
                $('#fvid').attr('src', result);
        });
        $.post("preparedness/loadpdf",{frame:frame},function(result){
                $('#pdf').attr('src', result);
        });
    });
})

/*==============================CONTACT PAGE==================================*/

/*Adding Contact*/

$(function(){
    $('#save-contact').click(function(){
        var cn_number = $('#cn_number').val(),
            cn_name = $('#cn_name').val(),
            cn_mail = $('#cn_mail').val(),
            cn_dept = $('#cn_dept').val(),
            cn_brgy = $('#cn_brgy').val();
        var cn1 = 0,
            cn2 = 0,
            cn3 = 0,
            cn4 = 0,
            cn5 = 0;
        var contactFormat =/^(\d{2})(\d{3})(\d{3})(\d{4})$/
		if(cn_number == "" || cn_number == " "){
			$('#notify-cnumber').show();
            $('#notify-cnumber').html("<li></li><li>Enter Contact Number</li>");
            cn1 = 0;
		}else{
			if(!contactFormat.test(cn_number)){
                $('#notify-cnumber').show();
                $('#notify-cnumber').html("<li></li><li>Invalid Contact Number</li>");
                cn1 = 0;
			}else{
				if(cn_number == "63" || cn_number == ""){
                    $('#notify-cnumber').show();
                    $('#notify-cnumber').html("<li></li><li>Enter Contact Number</li>");
                    cn1 = 0;
                }else{
                    $('#notify-cnumber').hide();
                    cn1 = 1;
                }
					
			}
		}
        
         if(cn_name == "" || cn_name == " "){
			$('#notify-name').show();
            $('#notify-name').html("<li></li><li>Enter Contact Name</li>");
            cn2 = 0;
		}else{
            $('#notify-name').hide();
            cn2 = 1;
        }
        var email_filter = /^.+@.+\..{2,3}$/
		var cn_mail = $('#cn_mail').val();
		if(cn_mail == "" || cn_mail == " "){
            $('#notify_email').show();
			$('#notify_email').html("<li></li><li>Enter a Valid Email Address</li>");
            cn3 = 0;
		}else{
			if(!email_filter.test(cn_mail)){
                 $('#notify_email').show();
			     $('#notify_email').html("<li></li><li>Invalid Email Address</li>");
                cn3 = 0;
			}else{
				 $('#notify_email').hide();
                cn3 = 1;
			}	
		}
        if(cn_dept == 0){
			$('#notify-dept').show();
            $('#notify-dept').html("<li></li><li>Select a Department</li>");
            cn4 = 0;
		}else{
            $('#notify-dept').hide();
            cn4 = 1;
        }
        if(cn_brgy == 0){
			$('#notify-brgy').show();
            $('#notify-brgy').html("<li></li><li>Select a Barangay</li>");
            cn5 = 0;
		}else{
            $('#notify-brgy').hide();
            cn5 = 1;
        }
        
        if((cn1 != 0) && (cn2 != 0) && (cn3 != 0) && (cn4 != 0) && (cn5 != 0)){
            $.post("addcontact/addnewcontact",{cn_number:cn_number,cn_name:cn_name,cn_mail:cn_mail,cn_dept:cn_dept,cn_brgy:cn_brgy},function(result){
                if(result == '1'){
                   $('#notify').fadeIn();
                   $('.notify-desc').html("Contact Saved!");    
                }else{
                    $('#notify').removeClass("notify-ok");
                    $('#notify').addClass("notify-err");
                    $('#notify').fadeIn();
                    $('.notify-desc').html("Some errors appeared...");
                }    
            });
        }else{
            $('#notify').removeClass("notify-ok");
            $('#notify').addClass("notify-err");
            $('#notify').fadeIn();
            $('.notify-desc').html("Some errors appeared...");
        }
    });
});

$(function(){
    $('#search-query').keyup(function(){
        $('.cn_loader').show();
        var contact_dept_search = $('#contact-dept-search').val(),
            search_query = $('#search-query').val();
        $.post("addcontact/querycontact",{contact_dept_search:contact_dept_search, search_query:search_query},function(result){
            console.log(result);
            $('#list-contact-result').html(result); 
        });
        $('.cn_loader').hide();
    }); 
})

window.addDashes = function addDashes(f) {
    var r = /(\D+)/g,
        npa = '',
        nxx = '',
        last4 = '';
    f.value = f.value.replace(r, '');
    npa = f.value.substr(2, 3);
    nxx = f.value.substr(5, 3);
    last4 = f.value.substr(8, 3);
    f.value ='63'+npa+nxx+last4;
}

$(function(){
    $('#cn_number').focus(function(){
		$('#cn_number').val("63");
	});
    $('#cn_number').keydown(function(){
		var cn_number = $('#cn_number').val();
		var lengthNumber = $('#cn_number').val().length;
		var key = event.keyCode || event.charCode;

		if(( key == 8 || key == 46 ) && (cn_number == "63")){
			return false;
		}
    });
    $('#cn_number').blur(function(){			
        var contactFormat = /^(\d{2})(\d{3})(\d{3})(\d{4})$/
		var cn_number = $('#cn_number').val();
        if(cn_number == "" || cn_number == " "){
			$('#notify-cnumber').show();
            $('#notify-cnumber').html("<li></li><li>Enter Contact Number</li>");
		}else{
			if(!contactFormat.test(cn_number)){
                $('#notify-cnumber').show();
                $('#notify-cnumber').html("<li></li><li>Invalid Contact Number</li>");
			}else{
				if(cn_number == "63"){
                    $('#notify-cnumber').show();
                    $('#notify-cnumber').html("<li></li><li>Enter Contact Number</li>");
                }else{
                    $('#notify-cnumber').hide();
                }
					
			}
		}
	});
    $('#cn_name').blur(function(){
         var cn_name = $('#cn_name').val();
        if(cn_name == "" || cn_name == " "){
			$('#notify-name').show();
            $('#notify-name').html("<li></li><li>Enter Contact Name</li>");
		}else{
            $('#notify-name').hide();
        }
    });
    $('#cn_mail').blur(function(){
		var email_filter = /^.+@.+\..{2,3}$/
		var cn_mail = $('#cn_mail').val();
		if(cn_mail == "" || cn_mail == " "){
            $('#notify_email').show();
			$('#notify_email').html("<li></li><li>Enter a Valid Email Address</li>");
		}else{
			if(!email_filter.test(cn_mail)){
                 $('#notify_email').show();
			     $('#notify_email').html("<li></li><li>Invalid Email Address</li>");
			}else{
				 $('#notify_email').hide();
			}	
		}
	});
    $('#cn_dept').change(function(){
         var cn_dept = $('#cn_dept').val();
        if(cn_dept == 0){
			$('#notify-dept').show();
            $('#notify-dept').html("<li></li><li>Select a Department</li>");
		}else{
            $('#notify-dept').hide();
        }
    });
    $('#cn_brgy').change(function(){
         var cn_brgy = $('#cn_brgy').val();
        if(cn_brgy == 0){
			$('#notify-brgy').show();
            $('#notify-brgy').html("<li></li><li>Select a Barangay</li>");
		}else{
            $('#notify-brgy').hide();
        }
    });
})